#ifndef _SIMPLE_FILE_HANDLER_
#define _SIMPLE_FILE_HANDLER_

// C linux includes
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <errno.h>

// C++ inlucdes
#include <iostream>

/**Return null if an error occurs.*/
char * readFile(const char * filename, unsigned * len);

/**Return false if an error occurs.*/
bool writeFile(const char * filename, char * writeBuffer, unsigned writeBufferSize, bool notExistCreate, bool appends);

#endif
