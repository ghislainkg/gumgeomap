#include "simple-file-handler.h"

char * readFile(const char * filename, unsigned * len)
{
    int fd = open(filename, 0, O_RDONLY);
    if(fd < 0) {
        std::cerr << "Fail opening file : \"" << filename << "\"" << std::endl;
        return nullptr;
    }

    struct stat fileStat;
    int res = stat(filename, &fileStat);
    if(res < 0) {
        std::cerr << "Fail accessing file infos : \"" << filename << "\"" << std::endl;
        return nullptr;
    }
    if((fileStat.st_mode & S_IFMT) == S_IFDIR) {
        std::cerr << "Cannot read directory : \"" << filename << "\"" << std::endl;
        return nullptr;
    }
    auto fileSize = fileStat.st_size;
    if(fileSize == 0) {
        return new char[1]{'\0'};
    }

    char * readBuffer = new char[fileSize+1];
    res = read(fd, readBuffer, fileSize);
    if(res < 0) {
        std::cerr << "Fail reading file : \"" << filename << "\"" << std::endl;
        return nullptr;
    }

    memcpy(readBuffer+fileSize, "\0", 1);

    *len = res;

    return readBuffer;
}

bool writeFile(const char * filename, char * writeBuffer, unsigned writeBufferSize, bool notExistCreate, bool appends)
{
    int flags = O_WRONLY;
    if(notExistCreate) {
        flags |= O_CREAT;
    }
    if(appends) {
        flags |= O_APPEND;
    }
    else {
        flags |= O_TRUNC;
    }
    int fd = open(filename, flags, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
    if(fd < 0) {
        std::cerr << "Fail opening file : |" << filename << "|" << std::endl;
        return false;
    }

    int res = write(fd, writeBuffer, writeBufferSize);
    if(res < 0) {
        std::cerr << "Fail writing file : |" << filename << "|" << std::endl;
        return false;
    }
    return true;
}
