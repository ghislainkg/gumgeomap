#include "client/app_client.h"
#include "client/mbuffer.h"
#include "filehandling/simple-file-handler.h"

#include <iostream>

/**
4 arguments :
- The file containing the json request
- The file that will containing the json response
- The name of the server
- The port of the server
*/
int main(int argc, char * argv[]) {

	/***********RETRIEVE ARGUMENTS*/
	if(argc < 5) {
		std::cout << "Need more" << std::endl;
		return 0;
	}
	char * requestFilename = argv[1];
	char * responseFilename = argv[2];
	char * serverHostname = argv[3];
	char * serverPort = argv[4];

	/***********READ REQUEST*/
	unsigned requestLen;
	char * requestStr = readFile(requestFilename, &requestLen);


	/***********CONNECT TO SERVER & SEND REQUEST*/
	ServerConnexion * connexion = create_server(serverPort, serverHostname);
	if(connexion == nullptr) {
		return 0;
	}
	int res = connect_to_server(*connexion);
	if(res < 0) {
		return 0;
	}
	res = send_msg_to_server(*connexion, requestStr, requestLen);
	if(res < 0) {
		return 0;
	}


	/***********RECEIVE RESPONSE*/
	std::cout << "Start waiting for answer" << std::endl;
	Buffer responseBuffer = create_buffer(256);
	size_t readLen = 0;
	char * swapBuffer = new char[256];
	res = receive_server_msg(*connexion, swapBuffer, 256, &readLen);
	if(res < 0) {
		return 0;
	}
	add_to_buffer(&responseBuffer, swapBuffer, readLen);
	std::cout << "Start reading answer" << std::endl;
	// wait content for 100 millis max
	while(check_fd(connexion->file_descriptor, 0, 100000, CHECK_FD_MODE_READ) == 1 && readLen > 0) {
		res = receive_server_msg(*connexion, swapBuffer, 256, &readLen);
		if(res < 0) {
			return 0;
		}
		std::cout << "P (" << readLen << "):" << swapBuffer << std::endl;
		add_to_buffer(&responseBuffer, swapBuffer, readLen);
	}


	/***********WRITE RESPONSE IN FILE*/
	if(!writeFile(responseFilename, responseBuffer.buf, responseBuffer.used_len, true, false)) {
		return 0;
	}


	/***********FREE RESOURCES*/
	destroy_buffer(responseBuffer);
	res = close_server_connexion(*connexion);
	if(res < 0) {
		return 0;
	}
	return 0;
}
