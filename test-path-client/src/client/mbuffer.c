#include "mbuffer.h"

Buffer create_buffer(size_t init_len) {
	char * buf = (char*) malloc(init_len);
	Buffer b =  {buf, 0, init_len};
	return b;
}

void add_to_buffer(Buffer * buffer, char * str, size_t str_len) {
	// On augmente la taille de la memoire si necessaire
	while(buffer->used_len + str_len >= buffer->len) {
		buffer->buf = (char*) realloc(buffer->buf, buffer->len * 2);
		buffer->len = buffer->len * 2;
	}
	memcpy(&(buffer->buf[buffer->used_len]), str, str_len);
	buffer->used_len += str_len;
}

void destroy_buffer(Buffer buffer) {
	free(buffer.buf);
	buffer.len = 0;
	buffer.used_len = 0;
}