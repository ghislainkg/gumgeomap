#ifndef _APP_CLIENT_
#define _APP_CLIENT_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

typedef struct ServerConnexion {
	int file_descriptor;
	struct sockaddr sock_addr;
} ServerConnexion;

#define MAX_MESSAGE_LEN 255

#define WAITING_TIME 3 // 3 secondes
#define CHECK_FD_MODE_READ 0
#define CHECK_FD_MODE_WRITE 1

/*Create a socket file descriptor and a sockaddr with "port" and "hostname" and bind them
Return NULL if it fail*/
ServerConnexion * create_server(char * port, char * hostname);

/*Connect to the server with the socket represented by connexion
Return -1 if it fail and 0 if not*/
int connect_to_server(ServerConnexion connexion);

/*Send a message
Return -1 if it fail*/
int send_msg_to_server(ServerConnexion connexion, char * message, size_t message_len);

/*Receive message
Return -1 if it fail*/
int receive_server_msg(ServerConnexion connexion, char * message, size_t message_buffer_len, size_t * message_res_len);

/*Close the connexion
Return -1 if it fail and 0 if not*/
int close_server_connexion(ServerConnexion connexion);

/*mode = 0 pour read (lecture)
mode = 1 pour write (ecriture)
Retourne :
-1 en cas d'erreur
0 quand l'operation est impossible
1 quand elle est possible
*/
int check_fd(int fd, int sec, int micsec, int mode);

#endif