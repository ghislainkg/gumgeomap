#include "app_client.h"

ServerConnexion * create_server(char * port, char * hostname) {

	struct addrinfo hints;
	memset(&hints, 0, sizeof(struct addrinfo));
	struct addrinfo *res;
	int result_getaddrinfo = getaddrinfo(hostname, port, &hints, &res);
	if(result_getaddrinfo != 0) {
		printf("Fail to getaddrinfo : %s", gai_strerror(result_getaddrinfo));
		return NULL;
	}

	struct sockaddr server_socket_addr = *(res->ai_addr);

	int domain = res->ai_family;
	int type = SOCK_STREAM;
	int protocol = 0;
	int server_socket_fd = socket(domain, type, protocol);
	if(server_socket_fd < 0) {
		perror("Fail to create socket file descriptor : ");
		return NULL;
	}

	int val = 1;
	int result_setsockopt = setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
	if(result_setsockopt < 0) {
		perror("Fail to set socket option SO_REUSEADDR : ");
	}

	ServerConnexion * connexion = (ServerConnexion *) malloc(sizeof(ServerConnexion));
	connexion->file_descriptor = server_socket_fd;
	connexion->sock_addr = server_socket_addr;
	
	freeaddrinfo(res);
	
	return connexion;
}

int connect_to_server(ServerConnexion connexion) {
	int result_connect = connect(connexion.file_descriptor, &connexion.sock_addr, sizeof(connexion.sock_addr));
	if(result_connect < 0) {
		perror("Fail to connect to server : ");
		return -1;
	}
	return 0;
}

int send_msg_to_server(ServerConnexion connexion, char * message, size_t message_len) {
	size_t len_write = write(connexion.file_descriptor, message, message_len);
	if(len_write < message_len) {
		perror("Fail to send message : ");
		return -1;
	}
	return 0;
}

int receive_server_msg(ServerConnexion connexion, char * message, size_t message_buffer_len, size_t * message_res_len) {
	size_t len_read = read(connexion.file_descriptor, message, message_buffer_len);
	if(len_read < 0) {
		perror("Fail to receive message : ");
		return -1;
	}
	*message_res_len = len_read;
	return 0;
}

int close_server_connexion(ServerConnexion connexion) {
	int how = SHUT_WR;
	int result_shutdown = shutdown(connexion.file_descriptor, how);
	if(result_shutdown < 0) {
		perror("Fail to shutdown file descriptor connexion : ");
		return -1;
	}
	int result_close = close(connexion.file_descriptor);
	if(result_close < 0) {
		perror("Fail to close connexion");
		return -1;
	}
	return 0;
}

int check_fd(int fd, int sec, int micsec, int mode) {
	fd_set rfds;
    struct timeval tv;
    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    tv.tv_sec = sec;
    tv.tv_usec = micsec;
   
    int retval;
    if(mode == 0) {
    	retval = select(fd+1, &rfds, NULL, NULL, &tv);
    }
    else {
    	retval = select(fd+1, NULL, &rfds, NULL, &tv);
    }
   
    if (retval == -1) {
        return -1;
    }
    else if (retval == 0) {
        return 0;
    }
    return 1;
}