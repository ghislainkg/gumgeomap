#include "help-functions.h"

#include "communication/parsing.h"
#include "communication/input-listener.h"

#include "display-map/display-map.h"

#include "path-request-manager/path-request-manager.h"

#include "config-manager/config-manager.h"

#define FILE_NAME_REQUEST "execution-data/query.txt"
#define FILE_NAME_REQUEST_RESPONSE "execution-data/response.xml"

void OnPathResponseFunction(
	std::vector<path_find::NodeResultList*> & pathList,
	osm_parsing::MatrixNode start,
    osm_parsing::MatrixNode end,
	osm_parsing::Matrix * matrix,
	bool useDisplay,
	void * data) {

	communication::Responder * responder = (communication::Responder *) ((void**)data)[0];
	configmanager::ConfigManager * config = (configmanager::ConfigManager *) ((void**)data)[1];
	unsigned carId = *( (unsigned*) ((void**)data)[2]);

	for(unsigned i=0; i<pathList.size(); i++) {
		std::cout << "Path " << i << std::endl;
		showPath(*(pathList[i]));
	}

	if(useDisplay) {
		display_map::DisplayMap * display = new display_map::DisplayMap(matrix);
		value_types::Distance minLat = getMinLatitude(matrix);
		value_types::Distance maxLat = getMaxLatitude(matrix);
		value_types::Distance minLon = getMinLongitude(matrix);
		value_types::Distance maxLon = getMaxLongitude(matrix);

		for(unsigned i=0; i<pathList.size(); i++) {
			display->addPath(pathList[i]);
		}

		std::vector<osm_parsing::MatrixNode> startEnd;
		startEnd.push_back(start);
		startEnd.push_back(end);
		display->setOtherNodes(startEnd);
		
		display->start(
			minLon, maxLon,
			minLat, maxLat,
			1100, 600);

		delete display;
	}

	std::vector<double> distanceList;
	std::vector<double> timeList;
	std::vector<unsigned> scoreList;

	for(unsigned pathIndex=0; pathIndex<pathList.size(); pathIndex++) {
		value_types::Distance totalDistance = computeTotalDistance(pathList[pathIndex]);
		value_types::Distance totalTime = computeTotalTimeHours(pathList[pathIndex]);
		distanceList.push_back(totalDistance);
		timeList.push_back(totalTime);

		char * json;
		unsigned len;
		communication::getRideJsonStr(
			pathList[pathIndex],
			totalDistance, totalTime, carId,
			&json, &len);

		// COMMUNICATION WITH PATH SCORE SERVER
		char * portStr = new char[10];
		char * addressStr = new char[256];
		sprintf(portStr, "%d", config->scoreServerPort());
		sprintf(addressStr, "%s", config->scoreServerAddress());
		communication::ServerConnexion * serverConnexion = 
			new communication::ServerConnexion(portStr, addressStr);
		delete portStr;
		delete addressStr;
		if(serverConnexion->connect_to_server() < 0) {
			responder->sendResponse(json, len);
			break;
		}
		if(serverConnexion->send_msg_to_server(json, len) < 0) {
			serverConnexion->close_server_connexion();
			responder->sendResponse(json, len);
			break;
		}
		unsigned scoreServerResponseMaxLen = 512;
		unsigned scoreServerResponseLen;
		char * scoreServerResponse = new char[scoreServerResponseMaxLen];
		memset(scoreServerResponse, 0, scoreServerResponseMaxLen);
		if(serverConnexion->receive_server_msg(scoreServerResponse, scoreServerResponseMaxLen, (size_t*)&scoreServerResponseLen) < 0) {
			serverConnexion->close_server_connexion();
			break;
		}
		serverConnexion->close_server_connexion();
		delete serverConnexion;
		// END 

		unsigned score = atoi(scoreServerResponse);
		scoreList.push_back(score);
		delete scoreServerResponse;
	}

	std::vector<unsigned> indexList = sortPathByScore(scoreList);

	char * finalJson;
	unsigned len;
	communication::getRideListJsonStr(
		pathList, scoreList, distanceList, timeList, indexList,
		&finalJson, &len);

	responder->sendResponse(finalJson, len);

	delete ((void**)data);
}

void OnPathResearchFailFunction(
	osm_parsing::Matrix * matrix,
	void * data)
{
	std::cout << "Error : Path research failed" << std::endl;

	communication::Responder * responder = (communication::Responder *) ((void**)data)[0];
	char * responseFail = (char *) "Error : Path research failed";
	responder->sendResponse(responseFail, strlen(responseFail));

	delete ((void**)data);
}

void onReceive(char * input, unsigned len, communication::Responder * responder, void * data){
	requestmanager::PathResquestManager * manager =
		(requestmanager::PathResquestManager *) data;

	std::cout << "RECEIVED : " << input << std::endl;

	long unsigned startId; double startLon; double startLat;
	long unsigned endId; double endLon; double endLat;
	unsigned carId;
	bool result = communication::parseRequest(
		input,
		startId, startLon, startLat,
		endId, endLon, endLat,
		carId);

	if(!result) {
		std::cout << "Error : Fail parsing inputs" << std::endl;
		char * responseFail = (char *) "Error : Fail parsing inputs";
		responder->sendResponse(responseFail, strlen(responseFail));
		return;
	}

	std::cout
		<< "startid = " << startId
		<< ", startlon = " << startLon
		<< ", startlat = " << startLat
		<< ", endid = " << endId
		<< ", endlon = " << endLon
		<< ", endlat = " << endLat
		<< std::endl;

	requestmanager::MatrixManagerRequestSettings requestSettings = {
		startId, // startID
    	endId,// arrivalID
    	startLon,// startLongitude
    	startLat,// startLatitude
    	endLon,// arrivalLongitude
    	endLat// arrivalLatitude
	};
	void ** args = new void*[3];
	args[0] = responder;
	args[1] = manager->getConfig();
	args[2] = new unsigned[1];
	*((unsigned*)args[2]) = carId;
	manager->addRequest(requestSettings, OnPathResponseFunction, OnPathResearchFailFunction, args);
}

int main(int argc, char *argv[]) {

	if(argc < 2) {
		std::cerr << "Need configuration file" << std::endl;
		return 0;
	}

	configmanager::ConfigManager config = configmanager::ConfigManager(argv[1]);

	requestmanager::PathResquestManager manager;
	manager.getMatrix()->setOptionalMaxNodeCount(config.maxNodeCount());
	manager.setConfig(&config);

	communication::InputListener * listener = new communication::InputListener(
		config.inputsPortNumber(),
		(communication::OnInputReceivedFunction) onReceive,
		(void*)&manager
	);
	listener->startListening();

	delete listener;

	return 0;
}
