#include "path-finder-types.h"

namespace path_find {

Node::Node() :
    distFromStart(osm_parsing::MATRIX_DISTANCE_INFINITY),
    father(nullptr),
    matrixNode(nullptr) {}

Node::Node(
        osm_parsing::MatrixNode * matrixNode,
        value_types::Distance distFromStart = osm_parsing::MATRIX_DISTANCE_INFINITY,
        Node * father = nullptr) :
    distFromStart(distFromStart),
    father(father),
    matrixNode(matrixNode) { }

NodePriorityQueue::NodePriorityQueue() {
    this->nodes = std::vector<Node*>();
}

void NodePriorityQueue::addNode(Node * node) {
    this->nodes.push_back(node);
    std::push_heap(this->nodes.begin(), this->nodes.end(), NodeCompareLambda);
}

Node * NodePriorityQueue::getMin() {
    std::pop_heap(this->nodes.begin(), this->nodes.end(), NodeCompareLambda);
    Node * min = this->nodes.back();
    this->nodes.pop_back();
    return min;
}

void NodePriorityQueue::update() {
    std::make_heap(this->nodes.begin(), this->nodes.end(), NodeCompareLambda);
}

double computeTotalDistance(NodeResultList * path) {
    if(path->size() <= 1)
        return 0.0;

    double distance = 0.0;
    for(unsigned i=1; i<path->size(); i++) {
        Node & node1 = path->at(i-1);
        Node & node2 = path->at(i);
        distance += geo_compute_cartesian_distance(
            node1.getMatrixNode()->getLatitude(),
            node1.getMatrixNode()->getLongitude(),
            node2.getMatrixNode()->getLatitude(),
            node2.getMatrixNode()->getLongitude());
    }

    return distance;
}

double computeTotalTimeHours(
    NodeResultList * path) {
    if(path->size() <= 1)
        return 0.0;

    double totalTime = 0.0;
    value_types::Distance d, v1, v2;
    for(unsigned i=1; i<path->size(); i++) {
        Node & node1 = path->at(i-1);
        Node & node2 = path->at(i);
        d = geo_compute_cartesian_distance(
            node1.getMatrixNode()->getLatitude(),
            node1.getMatrixNode()->getLongitude(),
            node2.getMatrixNode()->getLatitude(),
            node2.getMatrixNode()->getLongitude());

        v1 = node1.getMatrixNode()->getSpeed();
        v2 = node2.getMatrixNode()->getSpeed();
        // if(v1 == 0) v1 = defaultSpeed;
        // if(v2 == 0) v2 = defaultSpeed;

        totalTime += d*0.001 / ((v1+v2)/2.0);
    }

    return totalTime;
}

};