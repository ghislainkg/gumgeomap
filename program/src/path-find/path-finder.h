#ifndef _PATH_FINDER_
#define _PATH_FINDER_

#include "path-finder-types.h"

namespace path_find {

typedef value_types::Distance (*ArcComputeWeightFunction)(
    value_types::Distance speed,
    value_types::Distance distance,
    value_types::Distance price);

/**Classe effectuant l'algorithme de Dijkstra sur une matrice.*/
class PathFinder {
public:
    PathFinder(
        osm_parsing::Matrix * matrix,
        osm_parsing::MatrixNode * start,
        osm_parsing::MatrixNode * end);
    ~PathFinder();

    /**Lance l'algorithme et retourne le plus court chemin trouve.*/
    std::vector<Node> perform();

    inline void setOnlyWayElementAsExtraVoisin(bool value) {
        this->onlyWayElementAsExtraVoisin = value;
    }
    inline void setOnlyInWayAsExtraVoisin(bool value) {
        this->onlyInWayAsExtraVoisin = value;
    }
    inline void setArcComputeWeightFunction(ArcComputeWeightFunction function) {
        this->arcComputeWeightFunction = function;
    }

private:
    osm_parsing::Matrix * matrix;
    osm_parsing::MatrixNode * startNode;
    osm_parsing::MatrixNode * endNode;

    bool onlyWayElementAsExtraVoisin = true;
    bool onlyInWayAsExtraVoisin = true;

    /**La file de priorite*/
    NodePriorityQueue * priorityQueue;
    /**L'arbre de parcours et liste de distance.*/
    NodeResultList nodeResults;

    Node * nodeForEnd = nullptr;

    ArcComputeWeightFunction arcComputeWeightFunction = nullptr;

    void initialise();
    
    void majDistanceAndFather(
        Node * n1, Node * n2,
        osm_parsing::MatrixCase * edge);

    std::vector<Node> getPath();
};

};

#endif
