#ifndef _PARSING_
#define _PARSING_

#include "../vendors/rapidjson/document.h"
#include "../vendors/rapidjson/writer.h"
#include "../vendors/rapidjson/stringbuffer.h"

#include "../path-find/path-finder.h"

#include <iostream>

namespace communication {

bool parseGetLocation(
	rapidjson::Value & location, long unsigned & id, double & lon, double & lat);
bool parseRequest(char * req,
	long unsigned & startId, double & startLon, double & startLat,
	long unsigned & endId, double & endLon, double & endLat,
	unsigned & carId);

void getRideJsonStr(
	path_find::NodeResultList * path,
	double distance, double time, unsigned carId,
	char ** json, unsigned * len);
void getRideListJsonStr(
	std::vector<path_find::NodeResultList*> & pathList,
	std::vector<unsigned> & scores,
	std::vector<double> & distances,
	std::vector<double> & times,
	std::vector<unsigned> & indices,
	char ** json, unsigned * len);

}

#endif
