#include "parsing.h"

namespace communication {

bool parseGetLocation(
	rapidjson::Value & location, long unsigned & id, double & lon, double & lat) {

	if(location.IsObject()) {
		if(location.HasMember("osmId") && location["osmId"].IsUint64()) {
			id = location["osmId"].GetUint64();
			std::cout << "ID = " << id << std::endl;
		}
		else if(location.HasMember("osmId") && location["osmId"].IsUint()) {
			id = location["osmId"].GetUint();
			std::cout << "ID = " << id << std::endl;
		}
		if(location.HasMember("osmId") && location["osmId"].IsInt()) {
			id = location["osmId"].GetInt();
			std::cout << "ID = " << id << std::endl;
		}
		else {
			std::cout << "A" << std::endl;
			return false;
		}

		if(location.HasMember("latitude") && location["latitude"].IsDouble()) {
			lat = location["latitude"].GetDouble();
		}
		else {
			std::cout << "B" << std::endl;
			return false;
		}

		if(location.HasMember("longitude") && location["longitude"].IsDouble()) {
			lon = location["longitude"].GetDouble();
		}
		else {
			std::cout << "C" << std::endl;
			return false;
		}
	}
	else {
		return false;
	}

	return true;
}

bool parseRequest(char * req,
	long unsigned & startId, double & startLon, double & startLat,
	long unsigned & endId, double & endLon, double & endLat,
	unsigned & carId) {

	rapidjson::Document doc;
	doc.Parse(req);

	if(!doc.IsObject()) {
		std::cout << "D" << std::endl;
		return false;
	}

	if(doc.HasMember("startLocation")) {
		rapidjson::Value & loc = doc["startLocation"];
		parseGetLocation(loc, startId, startLon, startLat);
	}
	else {
		std::cout << "E" << std::endl;
		return false;
	}

	if(doc.HasMember("endLocation")) {
		rapidjson::Value & loc = doc["endLocation"];
		parseGetLocation(loc, endId, endLon, endLat);
	}
	else {
		std::cout << "F" << std::endl;
		return false;
	}

	if(doc.HasMember("idCar") && doc["idCar"].IsUint64()) {
		carId = doc["idCar"].GetUint64();
		std::cout << "CAR ID = " << carId << std::endl;
	}
	else if(doc.HasMember("idCar") && doc["idCar"].IsUint()) {
		carId = doc["idCar"].GetUint();
		std::cout << "CAR ID = " << carId << std::endl;
	}
	if(doc.HasMember("idCar") && doc["idCar"].IsInt()) {
		carId = doc["idCar"].GetInt();
		std::cout << "CAR ID = " << carId << std::endl;
	}
	else {
		std::cout << "G" << std::endl;
		return false;
	}

	return true;
}


static void getJsonDocString(rapidjson::Document & doc, char ** json, unsigned * len) {
	rapidjson::StringBuffer buffer;
	buffer.Clear();
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	doc.Accept(writer);
	*json = strdup(buffer.GetString());
	*len = buffer.GetSize();
}

static rapidjson::Value getPathJsonArray(path_find::NodeResultList * path, rapidjson::Document & doc) {
	rapidjson::Value array;
	array.SetArray();

	for(unsigned i=0; i<path->size(); i++) {
		path_find::Node & node = (*path)[i];
		osm_parsing::MatrixNode * matrixNode = node.getMatrixNode();

		rapidjson::Value n;
		n.SetObject();
		rapidjson::Value latitude;
		latitude.SetDouble(matrixNode->getLatitude());
		n.AddMember("latitude", latitude, doc.GetAllocator());

		rapidjson::Value longitude;
		longitude.SetDouble(matrixNode->getLongitude());
		n.AddMember("longitude", longitude, doc.GetAllocator());

		rapidjson::Value speed;
		speed.SetDouble(matrixNode->getSpeed());
		n.AddMember("speed", speed, doc.GetAllocator());

		rapidjson::Value osmId;
		osmId.SetUint(matrixNode->getId());
		n.AddMember("osmNodeId", osmId, doc.GetAllocator());

		array.PushBack(n, doc.GetAllocator());
	}

	return array;
}

static rapidjson::Value getCarRideObject(
	path_find::NodeResultList * path,
	double distance, double time, unsigned score, unsigned index,
	rapidjson::Document & doc
) {
	rapidjson::Value PATH = getPathJsonArray(path, doc);

	rapidjson::Value DIRECTION_PATH;
	DIRECTION_PATH.SetObject();
	DIRECTION_PATH.AddMember("path", PATH, doc.GetAllocator());

	rapidjson::Value Index;
	Index.SetUint(index);
	rapidjson::Value Distance;
	Distance.SetDouble(distance);
	rapidjson::Value Time;
	Time.SetDouble(time);
	rapidjson::Value Score;
	Score.SetUint(score);

	rapidjson::Value CAR_RIDE;
	CAR_RIDE.SetObject();
	CAR_RIDE.AddMember("index", Index, doc.GetAllocator());
	CAR_RIDE.AddMember("distance", Distance, doc.GetAllocator());
	CAR_RIDE.AddMember("time", Time, doc.GetAllocator());
	CAR_RIDE.AddMember("score", Score, doc.GetAllocator());
	CAR_RIDE.AddMember("directionPath", DIRECTION_PATH, doc.GetAllocator());

	return CAR_RIDE;
}

void getRideJsonStr(
	path_find::NodeResultList * path,
	double distance, double time, unsigned carId,
	char ** json, unsigned * len
) {
	rapidjson::Document doc;
	doc.SetObject();

	rapidjson::Value PATH = getPathJsonArray(path, doc);

	rapidjson::Value DIRECTION_PATH;
	DIRECTION_PATH.SetObject();
	DIRECTION_PATH.AddMember("path", PATH, doc.GetAllocator());

	rapidjson::Value Distance;
	Distance.SetDouble(distance);
	rapidjson::Value Time;
	Time.SetDouble(time);
	rapidjson::Value CarId;
	CarId.SetUint(carId);

	doc.AddMember("distance", Distance, doc.GetAllocator());
	doc.AddMember("time", Time, doc.GetAllocator());
	doc.AddMember("carId", CarId, doc.GetAllocator());
	doc.AddMember("directionPath", DIRECTION_PATH, doc.GetAllocator());

	getJsonDocString(doc, json, len);
}

void getRideListJsonStr(
	std::vector<path_find::NodeResultList*> & pathList,
	std::vector<unsigned> & scores,
	std::vector<double> & distances,
	std::vector<double> & times,
	std::vector<unsigned> & indices,
	char ** json, unsigned * len
) {
	rapidjson::Document doc;
	doc.SetObject();

	rapidjson::Value RIDES;
	RIDES.SetArray();
	
	for(unsigned i=0; i<pathList.size(); i++) {
		rapidjson::Value RIDE = getCarRideObject(
			pathList[i], distances[i], times[i], scores[i], indices[i], doc);
		RIDES.PushBack(RIDE, doc.GetAllocator());
	}

	doc.AddMember("rides", RIDES, doc.GetAllocator());

	getJsonDocString(doc, json, len);
}

}
