#ifndef _INPUT_LISTENER_
#define _INPUT_LISTENER_

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include <string>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#include <errno.h>

#define DEFAULT_MESSAGE_SIZE 256

namespace communication {

typedef struct Buffer {
	char * buf;
	size_t used_len;
	size_t len;
} Buffer;

Buffer create_buffer(size_t init_len);
void add_to_buffer(Buffer * buffer, char * str, size_t str_len);
void destroy_buffer(Buffer buffer);

#define CHECK_FD_MODE_READ 0
#define CHECK_FD_MODE_WRITE 1

class Responder {
public:
	Responder(int client_socket)
	: client_socket(client_socket) {}

	inline void sendResponse(char * response, unsigned len) {
		int result_write = write(client_socket, response, len);
		if(result_write < 0){
			std::cout << "Fail to write message : %s" << strerror(errno) << std::endl;
			return;
		}
		std::cout << "Response sent" << std::endl;
	}

private:
	int client_socket;
};

#define MAX_MESSAGE_LEN 255
class ServerConnexion {
public:
	ServerConnexion(char * port, char * hostname);

	int connect_to_server();

	int send_msg_to_server(char * message, size_t message_len);

	int receive_server_msg(char * message, size_t message_buffer_len, size_t * message_res_len);

	int close_server_connexion();

	int check_fd(int fd, int sec, int micsec, int mode);

	inline bool isReady() { return ready; }

private:
	int file_descriptor;
	struct sockaddr sock_addr;

	bool ready = false;
};

typedef void (*OnInputReceivedFunction)(char * input, unsigned len, Responder * responder, void * data);

class InputListener {
public:
	InputListener(
		unsigned port,
		OnInputReceivedFunction onInputReceivedFunction,
		void * data)
	: port(port), onInputReceivedFunction(onInputReceivedFunction), data(data) {}

	void startListening();

private:
	unsigned port;
	OnInputReceivedFunction onInputReceivedFunction;
	void * data;
};

}

#endif
