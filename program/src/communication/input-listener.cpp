#include "input-listener.h"

namespace communication {

Buffer create_buffer(size_t init_len) {
	char * buf = new char[init_len];
	Buffer b =  {buf, 0, init_len};
	return b;
}

void add_to_buffer(Buffer * buffer, char * str, size_t str_len) {
	// On augmente la taille de la memoire si necessaire
	while(buffer->used_len + str_len >= buffer->len) {
		char * aux = new char[buffer->len * 2];
		memcpy(aux, buffer->buf, buffer->len);
		buffer->buf = aux;
		buffer->len = buffer->len * 2;
	}
	memcpy(&(buffer->buf[buffer->used_len]), str, str_len);
	buffer->used_len += str_len;
}

void destroy_buffer(Buffer buffer) {
	delete buffer.buf;
	buffer.len = 0;
	buffer.used_len = 0;
}

int check_fd(int fd, int sec, int micsec, int mode) {
	fd_set rfds;
    struct timeval tv;
    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    tv.tv_sec = sec;
    tv.tv_usec = micsec;
   
    int retval;
    if(mode == 0) {
    	retval = select(fd+1, &rfds, NULL, NULL, &tv);
    }
    else {
    	retval = select(fd+1, NULL, &rfds, NULL, &tv);
    }
   
    if (retval == -1) {
        return -1;
    }
    else if (retval == 0) {
        return 0;
    }
    return 1;
}





ServerConnexion::ServerConnexion(char * port, char * hostname) {
	struct addrinfo hints;
	memset(&hints, 0, sizeof(struct addrinfo));
	struct addrinfo *res;
	int result_getaddrinfo = getaddrinfo(hostname, port, &hints, &res);
	if(result_getaddrinfo != 0) {
		printf("Fail to getaddrinfo : %s", gai_strerror(result_getaddrinfo));
		return;
	}

	struct sockaddr server_socket_addr = *(res->ai_addr);

	int domain = res->ai_family;
	int type = SOCK_STREAM;
	int protocol = 0;
	int server_socket_fd = socket(domain, type, protocol);
	if(server_socket_fd < 0) {
		perror("Fail to create socket file descriptor : ");
		return;
	}

	int val = 1;
	int result_setsockopt = setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
	if(result_setsockopt < 0) {
		perror("Fail to set socket option SO_REUSEADDR : ");
	}

	this->file_descriptor = server_socket_fd;
	this->sock_addr = server_socket_addr;
	
	freeaddrinfo(res);
	this->ready = true;
}

int ServerConnexion::connect_to_server() {
	int result_connect = connect(this->file_descriptor, &this->sock_addr, sizeof(this->sock_addr));
	if(result_connect < 0) {
		perror("Fail to connect to server : ");
		return -1;
	}
	return 0;
}

int ServerConnexion::send_msg_to_server(char * message, size_t message_len) {
	size_t len_write = write(this->file_descriptor, message, message_len);
	if(len_write < message_len) {
		perror("Fail to send message : ");
		return -1;
	}
	return 0;
}

int ServerConnexion::receive_server_msg(char * message, size_t message_buffer_len, size_t * message_res_len) {
	size_t len_read = read(this->file_descriptor, message, message_buffer_len);
	if(len_read < 0) {
		perror("Fail to receive message : ");
		return -1;
	}
	*message_res_len = len_read;
	return 0;
}

int ServerConnexion::close_server_connexion() {
	int how = SHUT_WR;
	int result_shutdown = shutdown(this->file_descriptor, how);
	if(result_shutdown < 0) {
		perror("Fail to shutdown file descriptor connexion : ");
		return -1;
	}
	int result_close = close(this->file_descriptor);
	if(result_close < 0) {
		perror("Fail to close connexion");
		return -1;
	}
	this->ready = false;
	return 0;
}

int ServerConnexion::check_fd(int fd, int sec, int micsec, int mode) {
	return check_fd(fd, sec, micsec, mode);
}



void InputListener::startListening() {
	// Obtention du descripteur de fichier pour le socket serveur
	int server_socket_fd;
	server_socket_fd = socket(PF_INET6, SOCK_STREAM, 0);
	if (server_socket_fd == -1) {
		std::cerr << "Fail to create socket : %s\n" << strerror(errno) << std::endl;
		return;
	}
	std::cout << "Input receiver server created" << std::endl;
	// Option pour rendre le port accessible directement après l'avoir libérer
	int val = 1;
	int result_setsockopt = setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
	if(result_setsockopt == -1) {
		std::cerr << "Fail to set reuse address option : %s\n" << strerror(errno) << std::endl;
		return;
	}

	// Création d'un socket en IPv6
	struct sockaddr_in6 sin6;
	memset(&sin6, 0, sizeof(sin6));
	sin6.sin6_family = PF_INET6;
	sin6.sin6_port = htons(port); // On fixe le port de communication

	// On bind notre socket à notre port
	int rc = bind(server_socket_fd, (struct sockaddr *)&sin6, sizeof(sin6));
	if(rc < 0) {
		std::cerr << "Fail to bind socket : %s\n" << strerror(errno) << std::endl;
		return;
	}
	std::cout << "Server binded" << std::endl;

	// Activation de l'écoute du socket du serveur
	std::cout << "Start listening for inputs" << std::endl;
	rc = listen(server_socket_fd, 1024);
	if(rc < 0) {
		std::cerr << "Fail to listen : %s\n" << strerror(errno) << std::endl;
		return;
	}

	while(1) {
		// Acceptation de nouvelle connexion au serveur (un nouveau socket client)
		int client_socket = accept(server_socket_fd, NULL, NULL);
		if(client_socket < 0) {
			std::cerr << "Fail to accept connexion : %s\n" << strerror(errno) << std::endl;
			std::cerr << "End\n\n\n"<< std::endl;
			break;
		}
		std::cout << "New input provider" << std::endl;

		Buffer message = create_buffer(DEFAULT_MESSAGE_SIZE*2);
		while(1) {
			// Lecture du message du client
			char * received_msg = new char[DEFAULT_MESSAGE_SIZE];
			int len = read(client_socket, received_msg, DEFAULT_MESSAGE_SIZE);
			if(len < 0) {
				std::cerr << "message not received : %s\n" << strerror(errno) << std::endl;
				break;
			}
			std::cout << "READ : |" << received_msg << "|" << std::endl;

			add_to_buffer(&message, received_msg, len);
			delete received_msg;

			if(check_fd(client_socket, 1, 0, CHECK_FD_MODE_READ) == 0 || len == 0) {
				add_to_buffer(&message, (char*)"\0", 1);
				std::cout << "ALL_READ : " << message.buf << std::endl;

				// char * t = "{\"startLocation\": {\"osmId\": 20, \"latitude\": 48.8530, \"longitude\": 2.3490}, \"endLocation\": {\"osmId\": 50, \"latitude\": 48.8537, \"longitude\": 2.3495}}";
				// Buffer testBuff = create_buffer(DEFAULT_MESSAGE_SIZE*2);
				// add_to_buffer(&testBuff, t, strlen(t));

				// std::cout << "PPPPPPP = " << testBuff.buf << std::endl;

				Responder responder = Responder(client_socket);
				// onInputReceivedFunction(testBuff.buf, testBuff.used_len, &responder, data);
				onInputReceivedFunction(message.buf, message.used_len, &responder, data);
				destroy_buffer(message);
				break;
			}
		}

		// On ferme le socket client (Le processus fils (client), et le processus père (serveur) ferme tous deux la connexion)
		// int result_close = close(client_socket);
		// if(result_close == -1){
		// 	std::cerr << "Fail to close socket : %s\n" << strerror(errno) << std::endl;
		// 	std::cerr << "End\n\n\n" << std::endl;
		// 	break;
		// }
		// std::cout << "End\n\n\n" << std::endl;
	}
}

}
