#ifndef _OSM_MATRIX_BUILDER_
#define _OSM_MATRIX_BUILDER_

#include "../utils/debug-stuff.h"
#include "../utils/performances.h"

#include <math.h>
#include <iostream>

#include "osm-xml-browser.h"
#include "osm-matrix.h"

namespace osm_parsing {

typedef double (*CalculBaseDistanceFunction)(
    const MatrixNode & fromNode,
    const MatrixNode & toNode,
    std::vector<types::Tag> & tags);

/** Définition de la classe permettant de construire une osm matrice
à partir d'un contenu xml. 
*/
class MatrixBuilder {

    /**Les fonctions amies qui modifierons les membres 
     * de classes pour construire la matrice.*/

    /**Fonction appelee quand le browser rencontre une node.*/
    friend void browseXmlOnNodeCallback(
        types::Node* n, MatrixBuilder * builder, Mutex *mutex);
    /**Fonction appelee quand le browser rencontre un way.*/
    friend void browseXmlOnWayCallback(
        types::Way* w, MatrixBuilder * builder, Mutex *mutex);
    /**Fonction appelee quand le browser rencontre une relation.*/
    friend void browseXmlOnRelationCallback(
        types::Relation* r, MatrixBuilder * builder, Mutex *mutex);

public:
    MatrixBuilder(const char * xmlCode);
    MatrixBuilder(const char * xmlCode,
        CalculBaseDistanceFunction calculBaseDistanceFunction);
    ~MatrixBuilder();

    inline void setMatrix(Matrix * matrix) {this->matrix = matrix;}
    void resetBrowser(const char * xmlCode);

    /**Lance l'interprétation du xml et la création de la matrice.
     * Retourne la matrice.
     * startMatrixSize est le nombre de nodes a allouer au depart
     * en prevision de la taille du xml.*/
    Matrix * build(unsigned int startMatrixSize);
    /**Lance l'interprétation du xml et ecris dans la matrice courant
     * sans la reinitialiser. Retourne la matrice.
     * extraAllocation est le nombre de node a alouer en plus en prevision de 
     * la taille du xml.*/
    Matrix * buildInCurrentMatrix(unsigned int extraAllocation);

    /**Retourne la matrice. Vaut nullptr avant l'appel de this->build().
     * La class ne delete pas la matrix.*/
    inline Matrix * getMatrix() const { return this->matrix; }

    /**Retourne le pointeur vers la fonction de calcul de distance 
     * entre deux nodes consecutifs d'un way.*/
    inline CalculBaseDistanceFunction getCalculBaseDistanceFunction() const
        { return this->calculBaseDistanceFunction; }
    /**Change le pointeur vers la fonction de calcul de distance 
     * entre deux nodes consecutifs d'un way.*/
    inline void setCalculBaseDistanceFunction(CalculBaseDistanceFunction function)
        { this->calculBaseDistanceFunction = function; }

    /**Retourne le nombre de nodes contenu dans la matrice.
     * Vaut 0 avant l'appel de this->build().*/
    inline value_types::Index getNodeCount() const { return this->matrix->getSize(); }

    inline void setCustomMutexForMatrixWritingBlock(Mutex * mutex) {
        this->browser->setOptionalMutex(mutex);
    }

    inline void setDefaultWaySpeed(value_types::Distance speed) {
        defaultSpeed = speed;
    }
    inline value_types::Distance getDefaultWaySpeed() {
        return defaultSpeed;
    }

    inline void setThreadCount(unsigned count) {
        threadCount = count;
    }

private:
    OsmXmlBrowser * browser;
    Matrix * matrix;

    value_types::Distance defaultSpeed = 30.0;
    unsigned threadCount = 1;

    /**La fonction de calcul de distance.*/
    CalculBaseDistanceFunction calculBaseDistanceFunction = nullptr;    
    /**La fonction de calcul de distance par défaut.*/
    double defaultCalculBaseDistance(
        const MatrixNode & fromNode,
        const MatrixNode & toNode,
        std::vector<types::Tag> & tags);
};

};

#endif
