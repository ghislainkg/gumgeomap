#include "osm-xml-browser-thread.h"

namespace osm_parsing {
    void mutexLock(Mutex * mutex) {pthread_mutex_lock(mutex);}
    void mutexUnlock(Mutex * mutex) {pthread_mutex_unlock(mutex);}

BrowserThread::BrowserThread(
    BrowserThreadHandler * handler,
    char * name,
    value_types::Index start,
    value_types::Index end,
    ThreadBrowsingFunction function,
    void * data
) : handler(handler), start(start), end(end), function(function), data(data), name(name) {
    this->cursor = 0;
}

void BrowserThread::step() {
    this->cursor++;

    if(this->cursor >= this->end) {

        // IMPORTANT : CE BLOCK DOIT ETRE UNE ZONE PROTEGEE

        if(this->handler->mutex == nullptr) {
            std::cerr << "BrowserThread : Cannot perform locked step action, mutex is null" << std::endl;
            std::cerr << "\tThread : " << name << cursor << std::endl;
            std::cerr << "\tcurrent cursor = " << cursor << std::endl;
            return;
        }

        mutexLock(this->handler->mutex);

        // On passe a la fin des index de thread et on la met a jour
        this->start = this->handler->currentEnd;
        this->cursor = this->start;
        this->handler->currentEnd += this->handler->rangeSpan;
        this->end = this->handler->currentEnd;

        mutexUnlock(this->handler->mutex);
    }
}

bool BrowserThread::shouldWork() {
    if(this->cursor >= this->start && this->cursor < this->end) {
        return true;
    }
    return false;
}

Mutex * BrowserThread::getMutex() {
    return this->handler->mutex;
}

void * mainThreadFunction(BrowserThread * thread) {    
    thread->function(*thread, thread->data);
    return nullptr;
}

void BrowserThread::work() {
    int result = pthread_create(
        &(this->thread), nullptr,
        (void*(*)(void*))mainThreadFunction, this);
    if(result != 0) {
        switch(result) {
        case EAGAIN:
            std::cerr << "BrowserThread : Not enough resources to create thread : " << this->name << std::endl;
            break;
        default:
            std::cerr << "BrowserThread : An error occured while creating thread : " << this->name << std::endl;
        }
    }
}

BrowserThreadHandler::BrowserThreadHandler(
    value_types::Index rangeSpan,
    unsigned int threadCount,
    ThreadBrowsingFunction function,
    void * data, Mutex * mutex
) : rangeSpan(rangeSpan), threadCount(threadCount) {
    this->threads = std::vector<BrowserThread>();
    this->threads.reserve(threadCount);

    this->mutex = mutex;
    isDefaultMutex = false;

    this->spreadWork(rangeSpan, threadCount, function, data);
}

BrowserThreadHandler::BrowserThreadHandler(
    value_types::Index rangeSpan,
    unsigned int threadCount,
    ThreadBrowsingFunction function,
    void * data
) : BrowserThreadHandler(rangeSpan, threadCount, function, data, nullptr) {
    this->mutex = new Mutex();
    *(this->mutex) = PTHREAD_MUTEX_INITIALIZER;
    isDefaultMutex = true;
}

void BrowserThreadHandler::spreadWork(
    value_types::Index rangeSpan,
    unsigned int threadCount,
    ThreadBrowsingFunction function,
    void * data
) {
    unsigned int i;
    for(i=0; i<threadCount; i++) {
        char * name = new char[10];
        sprintf(name, "BrowserThread N.%d", i);

        BrowserThread thread = BrowserThread(
            this,
            name,
            rangeSpan*i,
            rangeSpan*(i+1)-1,
            function,
            data
        );

        this->threads.push_back(thread);
    }
    this->currentEnd = rangeSpan*(i);
}

void BrowserThreadHandler::work() {
    for(auto & thread : this->threads) {
        thread.work();
    }

    for(auto & thread : this->threads) {
        pthread_join(thread.thread, nullptr);

        delete thread.getName();
    }
}

};