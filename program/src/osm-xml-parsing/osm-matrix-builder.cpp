#include "osm-matrix-builder.h"

namespace osm_parsing {

MatrixBuilder::MatrixBuilder(
        const char * xmlCode) {
    this->browser = new OsmXmlBrowser(xmlCode);
    this->matrix = nullptr;
}

MatrixBuilder::MatrixBuilder(
        const char * xmlCode,
        CalculBaseDistanceFunction calculBaseDistanceFunction) {
    this->browser = new OsmXmlBrowser(xmlCode);
    this->matrix = nullptr;
    this->calculBaseDistanceFunction = calculBaseDistanceFunction;
}

MatrixBuilder::~MatrixBuilder() {
    delete this->browser;
}

void MatrixBuilder::resetBrowser(const char * xmlCode) {
    delete this->browser;
    this->browser = new OsmXmlBrowser(xmlCode);
}

void browseXmlOnNodeCallback(
    types::Node* n,
    MatrixBuilder * builder,
    Mutex *mutex)
{
    if(mutex == nullptr) {
        std::cerr << "Cannot add node : Matrix browse mutex is null" << std::endl;
        return;
    }
    if(builder == nullptr) {
        std::cerr << "Cannot add node : Matrix builder is nul" << std::endl;
        return;
    }

    MatrixNode node(n->id, n->lon, n->lat, 0, 0);
    for(unsigned i=0; i<n->tags.size(); i++) {
        if(n->tags[i].key == types::TagKey::HIGHWAY) {
            node.setIsWayElement(true);
            break;
        }
    }

    // DEBUG(std::cout << "Node : " << node.getId() <<
    //     " lat=" << node.getLatitude() <<
    //     " lon=" << node.getLongitude() <<
    //     std::endl;)

    mutexLock(mutex);
    builder->getMatrix()->addNode(node);
    mutexUnlock(mutex);
}
void browseXmlOnWayCallback(
    types::Way* w,
    MatrixBuilder * builder,
    Mutex *mutex
) {
    if(mutex == nullptr) {
        std::cerr << "Cannot add way : Matrix browse mutex is null" << std::endl;
        return;
    }
    if(builder == nullptr) {
        std::cerr << "Cannot add way : Matrix builder is nul" << std::endl;
        return;
    }

    //DEBUG(std::cout << "Way" << std::endl;)
    auto nodes = w->nds;
    value_types::Index prevId = value_types::MAX_ID;
    for(auto currentId : nodes) {
        if(prevId == value_types::MAX_ID) {
            prevId = currentId;
            continue;
        }

        // On recupere les node pour le calcul de distance de base
        MatrixNode * prevNode, * currentNode;
        prevNode = 
            builder->getMatrix()->getNodeById(prevId);
        if(prevNode == nullptr) {
            // Le node est encore inconnue
            std::cerr << "Cannot add way : Prev node not in matrix" << std::endl;
            prevId = currentId;
            continue;
        }
        currentNode = builder->getMatrix()->getNodeById(currentId);
        if(currentNode == nullptr) {
            // Le node est encore inconnue
            std::cerr << "Cannot add way : Current node not in matrix" << std::endl;
            continue;
        }

        // DEBUG(
        //     std::cout << "\tmade of Node : " << prevNode->getId() <<
        //     " lat=" << prevNode->getLatitude() <<
        //     " lon=" << prevNode->getLongitude() <<
        //     std::endl;
        //     std::cout << "\tto Node : " << currentNode->getId() <<
        //     " lat=" << currentNode->getLatitude() <<
        //     " lon=" << currentNode->getLongitude() <<
        //     std::endl;
        // )

        // TODO : Que faire avec les tags ?

        // On calcule la distance de base
        MatrixCase c;
        if(builder->getCalculBaseDistanceFunction() == nullptr)
            c.setDistance(builder->defaultCalculBaseDistance(
                *prevNode, *currentNode,
                w->tags));
        else
            c.setDistance(builder->getCalculBaseDistanceFunction()(
                    *prevNode, *currentNode,
                    w->tags));

        // On ajoute l'arc a la matrice
        auto casePointer = builder->getMatrix()->setCaseByIds(
            prevId, currentId, c);
        casePointer = builder->getMatrix()->setCaseByIds(
            currentId, prevId, c); // Double direction

        MatrixNode * nodeFrom = builder->getMatrix()->getNode(casePointer->getIndexFrom());
        MatrixNode * nodeTo = builder->getMatrix()->getNode(casePointer->getIndexTo());
        value_types::Distance speed = retrieveSpeedInFranceFromTags(w->tags, builder->getDefaultWaySpeed()); 
        nodeFrom->setSpeed(speed);
        nodeTo->setSpeed(speed);

        for(unsigned i=0; i<w->tags.size(); i++) {
            if(w->tags[i].key == types::TagKey::HIGHWAY) {
                nodeTo->setIsInWay(true);
                nodeFrom->setIsInWay(true);
            }
        }

        prevId = currentId;
    }
}
void browseXmlOnRelationCallback(
    types::Relation* r,
    MatrixBuilder * builder,
    Mutex *mutex) {

}

double MatrixBuilder::defaultCalculBaseDistance(
    const MatrixNode & fromNode,
    const MatrixNode & toNode,
    std::vector<types::Tag> & tags) {

    return geo_compute_cartesian_distance(
        fromNode.getLatitude(), fromNode.getLongitude(),
        toNode.getLatitude(), toNode.getLongitude());
}

Matrix * MatrixBuilder::build(unsigned int startMatrixSize) {
    this->matrix = new Matrix(0);
    this->matrix->clear();

    return this->buildInCurrentMatrix(startMatrixSize);
}

Matrix * MatrixBuilder::buildInCurrentMatrix(unsigned int extraAllocation) {
    this->browser->setThreadCount(this->threadCount);

    DEBUG(std::cout << "Start Building" << std::endl;)
    // On commence à 0 nodes dans la matrice
    this->browser->browseParallel(
        (BrowseXmlOnNodeCallback) browseXmlOnNodeCallback,
        (BrowseXmlOnWayCallback) browseXmlOnWayCallback,
        (BrowseXmlOnRelationCallback) browseXmlOnRelationCallback,
        this);
    DEBUG(std::cout << "End Building" << std::endl;)

    return this->matrix;
}

};
