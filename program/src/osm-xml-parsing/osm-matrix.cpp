#include "osm-matrix.h"

namespace osm_parsing {

Matrix::Matrix(value_types::Index size) {
    this->size = 0;
    this->nodes = std::vector<MatrixNode>();
    this->nodes.reserve(size);
    this->cases = std::vector<MatrixCase>();
    this->cases.reserve(size*size);

    // this->nodes.insert(
    //     this->nodes.end(),
    //     size,
    //     MatrixNode(0, 0, 0, 0)
    // );
    // this->cases.insert(
    //     this->cases.end(),
    //     size*size,
    //     MatrixCase(0, 0, 0)
    // );

    /**TODO : use clear (memset)*/
    // clear();

    this->allocSize = size;
}

Matrix::~Matrix() {}

// static void checkMatrixSizes(
//         std::vector<MatrixNode> nodes,
//         std::vector<MatrixCase> cases,
//         value_types::Index size) {
//     if(nodes.size() != size) {
//         std::cerr << "Nodes count don't match matrix size. nodes.size = " 
//             << nodes.size()
//             << "matrix size = " << size << std::endl;
//         DEBUG(assert(false);)
//     }
//     if(cases.size() != size*size) {
//         std::cerr << "Cases count don't match matrix size*size. cases.size = " 
//             << cases.size()
//             << "matrix size*size = " << size*size << std::endl;
//         DEBUG(assert(false);)
//     }
// }

value_types::Index Matrix::getCaseIndex(
        value_types::Index i, value_types::Index j) const {
    
    value_types::Index res;
    if(j<=i) {
        res = i*i + j;
    }
    else {
        res = (j-1)*(j-1) + (2*j+1) - i;
    }

    if(res < this->size*this->size)
        return res;
    else
        return MATRIX_NO_INDEX;
}

value_types::Index Matrix::getNodeIndex(
        value_types::Id id) const {
    for(value_types::Index i=0; i<this->size; i++) {
        if(this->nodes[i].getId() == id) {
            return i;
        }
    }
    return MATRIX_NO_INDEX;
}

MatrixCase * Matrix::getCase(
        value_types::Index i, value_types::Index j) const {
    value_types::Index index = getCaseIndex(i, j);
    if(index == MATRIX_NO_INDEX)
        return nullptr;
    return (MatrixCase *) &(this->cases[index]);
}

MatrixCase * Matrix::setCase(
        value_types::Index i, value_types::Index j,
        const MatrixCase & c) {
    value_types::Index index = getCaseIndex(i, j);
    if(index == MATRIX_NO_INDEX)
        return nullptr;
    this->cases[index] = c;
    this->cases[index].setIndexFrom(i);
    this->cases[index].setIndexTo(j);
    return (MatrixCase *) &(this->cases[index]);
}

MatrixCase * Matrix::getCaseByIds(
        value_types::Id idFrom, value_types::Id idTo) const {
    value_types::Index indexFrom = getNodeIndex(idFrom);
    value_types::Index indexTo = getNodeIndex(idTo);
    if(indexFrom == MATRIX_NO_INDEX || indexTo == MATRIX_NO_INDEX)
        return nullptr;
    return (MatrixCase *) &(this->cases[getCaseIndex(indexFrom, indexTo)]);
}

MatrixCase * Matrix::setCaseByIds(
        value_types::Id idFrom, value_types::Id idTo,
        const MatrixCase & c) {
    value_types::Index indexFrom = getNodeIndex(idFrom);
    value_types::Index indexTo = getNodeIndex(idTo);
    if(indexFrom == MATRIX_NO_INDEX || indexTo == MATRIX_NO_INDEX)
        return nullptr;
    auto caseIndex = getCaseIndex(indexFrom, indexTo);
    this->cases[caseIndex] = c;
    this->cases[caseIndex].setIndexFrom(indexFrom);
    this->cases[caseIndex].setIndexTo(indexTo);

    // std::cout << "from : " << indexFrom << " to : " << indexTo << std::endl;
    // std::cout << "Case : " << getCaseIndex(indexFrom, indexTo) << std::endl;
    // this->cases[getCaseIndex(indexFrom, indexTo)] = 100;

    return (MatrixCase *) &(this->cases[caseIndex]);
}

MatrixNode * Matrix::getNodeById(
        value_types::Id id) const {
    value_types::Index index = getNodeIndex(id);
    if(index == MATRIX_NO_INDEX)
        return nullptr;
    else
        return (MatrixNode *) &(this->nodes[index]);
}

MatrixNode * Matrix::getNode(
        value_types::Index i) const {
    if(i < this->size)
        return (MatrixNode*) &(this->nodes[i]);
    return nullptr;
}

MatrixNode * Matrix::getNodeCloseTo(
        value_types::Distance lat, value_types::Distance lon,
        bool onlyWayElement, bool onlyInWay,
        value_types::Distance maxDistance) const {

    MatrixNode* closest = (MatrixNode*) &(this->nodes[0]);
    value_types::Distance closestDistance = geo_compute_cartesian_distance(
        nodes[0].getLatitude(), nodes[0].getLongitude(),
        lat, lon);

    for(unsigned i=1; i<this->size; i++) {
        value_types::Distance distance = geo_compute_cartesian_distance(
            nodes[i].getLatitude(), nodes[i].getLongitude(),
            lat, lon);

        if(maxDistance > distance && closestDistance > distance
            && ( (!(onlyWayElement && onlyInWay)) || (onlyWayElement && nodes[i].getIsWayElement()) || (onlyInWay && nodes[i].getIsInWay()) )) {
            closest = (MatrixNode*) &(this->nodes[i]);
            closestDistance = distance;
        }
    }
    return closest;
}

std::vector<MatrixNode *> Matrix::getNodesSameLocation(MatrixNode * n, bool onlyWayElement, bool onlyInWay) const {
    std::vector<MatrixNode *> asCloseNodes;

    for(unsigned i=1; i<this->size; i++) {
        value_types::Distance distance = geo_compute_cartesian_distance(
            nodes[i].getLatitude(), nodes[i].getLongitude(),
            n->getLatitude(), n->getLongitude());

        if(distance <= this->nullDistanceApproximation && nodes[i].getId() != n->getId()
            && ( (!(onlyWayElement && onlyInWay)) || (onlyWayElement && nodes[i].getIsWayElement()) || (onlyInWay && nodes[i].getIsInWay()) )) {
            asCloseNodes.push_back((MatrixNode*) &(nodes[i]));
        }
    }
    return asCloseNodes;
}

MatrixNode * Matrix::setNode(
        value_types::Index i,
        const MatrixNode & n) {
    if(i < this->size) {
        this->nodes[i] = n;
        this->nodes[i].setIndex(i);
        return (MatrixNode*) &(this->nodes[i]);
    }
    return nullptr;
}

bool Matrix::allocExtra(value_types::Index extraNodeCount) {
    if(this->size >= optionalMaxNodeCount) {
        std::cerr << "Matrix Optional max node count reach :" << this->size << std::endl;
        return false;
    }

    if(nodes.max_size() <= allocSize + extraNodeCount) {
        std::cerr << "Matrix : Cannot allocate more space, Max node array capacity reach" << std::endl;
        std::cerr << "\tMax = " << nodes.max_size() << std::endl;
        std::cerr << "\tCurrent = " << allocSize << std::endl;
        std::cerr << "\trequested extra allocation = " << extraNodeCount << std::endl;
        return false;
    }
    this->allocSize += extraNodeCount;
    try {
        this->nodes.reserve(this->allocSize);
        this->cases.reserve(this->allocSize*this->allocSize);
    }
    catch(std::bad_alloc & e) {
        std::cerr << "Matrix : Exception while allocating more space " << std::endl;
        std::cerr << "Matrix : Exception : " << e.what() << std::endl;
        return false;
    }

    return true;
}

bool Matrix::allocMore() {
    if(this->size >= this->allocSize) {
        if(this->allocExtraSize == 0) {
            std::cerr << "Matrix : Extra allocation size is = 0" << std::endl;
            return true;
        }
        return allocExtra(this->allocExtraSize);
    }
    return true;
}
bool Matrix::addNode(const MatrixNode & node) {
    if(this->size >= optionalMaxNodeCount) {
        std::cerr << "Matrix Optional max node count reach :" << this->size << std::endl;
        return false;
    }

    if(getNodeById(node.getId()) != nullptr) {
        return true;
    }
    
    if(!allocMore()) {
        return false;
    }

    std::cout << "Matrix size = " << size << std::endl;

    // Ajoutons le node
    this->size++;
    nodes.push_back(node);
    nodes[size-1].setIndex(size-1);
    cases.resize(size*size, MatrixCase(0, 0, 0));
    return true;
}

std::vector<Voisin> * Matrix::getVoisins(
        MatrixNode * n) const {
    
    if(n == nullptr) {
        std::cerr << "Matrix : cannot return voisins of null node" << std::endl;
        return nullptr;
    }

    std::vector<Voisin> * res = new std::vector<Voisin>();
    value_types::Index j = getNodeIndex(n->getId());
    if(j == MATRIX_NO_INDEX) {
        std::cerr << "Matrix : cannot return voisins of node not in matrix" << std::endl;
        return nullptr;
    }
    for(value_types::Index i = 0; i < this->size; i++) {
        MatrixCase * c = getCase(j, i);
        if(c->getDistance() < MATRIX_DISTANCE_INFINITY && c->getDistance() > 0) {
            auto n = getNode(i);
            res->emplace_back(n, c);
        }
    }

    return res;
}

void Matrix::clear() {
    memset(static_cast<void*>(this->nodes.data()), 0, this->size);
    memset(static_cast<void*>(this->cases.data()), 0, this->size*this->size);
}

void Matrix::showNodes(
        value_types::Index usedFromIndex,
        value_types::Index usedToIndex) const {
    /**Affichage des nodes que contient la matrice.*/
    for(value_types::Index i=usedFromIndex; i<usedToIndex; i++) {
        MatrixNode * node = this->getNode(i);
        std::cout << "Node " << node->getId() <<
            ": lat=" << node->getLatitude() <<
            " lon=" << node->getLongitude() <<
            " speed=" << node->getSpeed() <<
            std::endl;
    }
}

void Matrix::showContent() const {
    for(value_types::Index i=0; i<this->size; i++) {
        for(value_types::Index j=0; j<this->size; j++) {
            MatrixCase * c = getCase(i, j);
            std::cout << c->getDistance() << "  ";
        }
        std::cout << std::endl;
    }
}

void Matrix::showRawContent() const {
    for(value_types::Index i=0; i<this->size*this->size; i++) {
        std::cout << this->cases.data()[i].getDistance() << " ";
    }
    std::cout << std::endl;
}

}
