#ifndef _OSM_REQUEST_
#define _OSM_REQUEST_

#include "../utils/debug-stuff.h"
#include "request.h"

// For HTTPS
#include <curl/curl.h>
#include <string>
#include <string.h>
#include <iostream>

namespace osm_request {

    //static const std::string DEFAULT_OSM_URL = "https://z.overpass-api.de/api/interpreter";
    //static const std::string DEFAULT_OSM_URL = "https://lz4.overpass-api.de/api/interpreter";
    //static const std::string DEFAULT_OSM_URL = "https://overpass.osm.ch/api/interpreter";
    //static const std::string DEFAULT_OSM_URL = "https://overpass-api.de/api/interpreter";
    static const std::string DEFAULT_OSM_URL = "http://overpass.openstreetmap.fr/api/interpreter";
    //static const std::string DEFAULT_OSM_URL = "https://overpass.nchc.org.tw/api/interpreter";
    
    static const std::string DEFAULT_OSM_REQUEST = "node(48.8534,2.3487,48.8539,2.3492)[highway=primary]; <; >; out;";

/**Lance une requete Overpass et retourne le xml de la reponse.*/
std::string query_xml_map(std::string osm_request, std::string osm_url);

std::string chooseOsmUrl();
double chooseRayon(
    double startLat, double startLon, double arrivalLat, double arrivalLon);
HighwayTag chooseHighwayTag(
    double startLat, double startLon, double arrivalLat, double arrivalLon);
std::string performRequest(
    double latMin, double lonMin, double latMax, double lonMax);

/**This function check wether an osm response contains node or an error.
 * Is performed in constant time (do not read the whole file)>
*/
bool isOsmResponseOk(std::string osmResponse);

};

#endif
