#ifndef _REQUEST_
#define _REQUEST_

#include <iostream>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>

#include "../utils/value-types.h"

typedef enum {
  MOTORWAY,
  TRUNK,
  PRIMARY,
  SECONDARY,
  TERTIARY,
  ROAD,
  RESIDENTIAL,
  UNCLASSIFIED,
  MOTORWAY_LINK,
  TRUNK_LINK,
  PRIMARY_LINK,
  SECONDARY_LINK,
  TERTIARY_LINK
} HighwayTag;

#define OVERPASS_WAY_BLOCK_SIZE 1024
#define OVERPASS_HIGHWAY_VALUE_SIZE 64

typedef struct
{
	double radius;
	double centerLat;
	double centerLon;
	HighwayTag highwayTag;
} WayAroundBlockSetting;

typedef struct
{
	double latStart;
	double lonStart;
	double radiusStart;
	
	double latMiddle;
	double lonMiddle;
	double radiusMiddle;

	double latEnd;
	double lonEnd;
	double radiusEnd;

	HighwayTag * highwayStartZoneTags;
	unsigned highwayStartZoneTagsCount;
	HighwayTag * highwayMiddleZoneTags;
	unsigned highwayMiddleZoneTagsCount;
	HighwayTag * highwayEndZoneTags;
	unsigned highwayEndZoneTagsCount;
} OverpassRequestSetting;

char * overpass_get_highway_value(
	HighwayTag highwayTag);

char * overpass_get_way_around_block(
	WayAroundBlockSetting & settings);
char * overpass_get_ways_aggreg_block(
	WayAroundBlockSetting * wayBlocks, unsigned wayBlockCount);
char * overpass_get_ways_tags_aggreg_block(
  double lat, double lon, double radius,
  HighwayTag * highwayTags, unsigned highwayTagCount,
  char * blockName);
char * overpass_get_request_route(
	OverpassRequestSetting & setting);
char * requeteRoute(
	double latStart, double lonStart,
	double latEnd, double lonEnd);


typedef struct
{
	double lat;
	double lon;
	double radius;
	HighwayTag * highwayTags;
	unsigned highwayTagsCount;
} OverpassZoneRequestSetting;

char * overpass_get_request_route_with_multi_zones(
	OverpassZoneRequestSetting * zonesSettings, unsigned zoneCount);

char * requeteRouteMultiZones(
	double latStart, double lonStart,
	double latEnd, double lonEnd);

#endif
