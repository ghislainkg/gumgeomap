#include "request.h"

using namespace std;

char * overpass_get_highway_value(HighwayTag highwayTag) {
  char * value = new char[OVERPASS_HIGHWAY_VALUE_SIZE];
  switch(highwayTag) {
    case MOTORWAY:
      sprintf(value, "motorway");
      break;
    case PRIMARY:
      sprintf(value, "primary");
      break;
    case SECONDARY:
      sprintf(value, "secondary");
      break;
    case TERTIARY:
      sprintf(value, "tertiary");
      break;
    case TRUNK:
      sprintf(value, "trunk");
      break;
    case ROAD:
      sprintf(value, "road");
      break;
    case RESIDENTIAL:
      sprintf(value, "residential");
      break;
    case UNCLASSIFIED:
      sprintf(value, "unclassified");
      break;
    case MOTORWAY_LINK:
      sprintf(value, "motorway_link");
      break;
    case TRUNK_LINK:
      sprintf(value, "trunk_link");
      break;
    case PRIMARY_LINK:
      sprintf(value, "primary_link");
      break;
    case SECONDARY_LINK:
      sprintf(value, "secondary_link");
      break;
    case TERTIARY_LINK:
      sprintf(value, "tertiary_link");
      break;
  }
  return value;
}

char * overpass_get_way_around_block(WayAroundBlockSetting & settings) {
  char * block = new char[OVERPASS_WAY_BLOCK_SIZE];
  char * highwayTagValue = overpass_get_highway_value(settings.highwayTag);
  sprintf(block, "way(around:%f,%f,%f)[highway=%s];", 
    settings.radius,
    settings.centerLat,
    settings.centerLon,
    highwayTagValue);
  delete highwayTagValue;
  return block;
}

char * overpass_get_ways_aggreg_block(WayAroundBlockSetting * wayBlocks, unsigned wayBlockCount, char * blockName) {
  char * aggreg = new char[OVERPASS_WAY_BLOCK_SIZE*wayBlockCount];
  sprintf(aggreg, "(");
  unsigned cursor = 1;
  for(unsigned i=0; i<wayBlockCount; i++) {
    char * wayBlock = overpass_get_way_around_block(wayBlocks[i]);
    sprintf(aggreg+cursor, "%s", wayBlock);
    cursor += strlen(wayBlock);
    delete wayBlock;
  }
  sprintf(aggreg+cursor, ")->.%s;", blockName);
  return aggreg;
}

char * overpass_get_ways_tags_aggreg_block(
  double lat, double lon, double radius,
  HighwayTag * highwayTags, unsigned highwayTagCount,
  char * blockName)
{
  char * aggreg = new char[OVERPASS_WAY_BLOCK_SIZE*highwayTagCount];
  sprintf(aggreg, "(");
  unsigned cursor = 1;
  for(unsigned i=0; i<highwayTagCount; i++) {
    WayAroundBlockSetting settings = {radius, lat, lon, highwayTags[i]};
    char * wayBlock = overpass_get_way_around_block(settings);
    sprintf(aggreg+cursor, "%s", wayBlock);
    cursor += strlen(wayBlock);
    delete wayBlock;
  }
  sprintf(aggreg+cursor, ")->.%s;", blockName);
  return aggreg;
}

char * overpass_get_request_route(OverpassRequestSetting & setting) {
  char * startZone = overpass_get_ways_tags_aggreg_block(
    setting.latStart, setting.lonStart, setting.radiusStart,
    setting.highwayStartZoneTags, setting.highwayStartZoneTagsCount,
    (char*)"startw");

  char * endZone = overpass_get_ways_tags_aggreg_block(
    setting.latEnd, setting.lonEnd, setting.radiusEnd,
    setting.highwayEndZoneTags, setting.highwayEndZoneTagsCount,
    (char*)"endw");

  char * middleZone = overpass_get_ways_tags_aggreg_block(
    setting.latMiddle, setting.lonMiddle, setting.radiusMiddle,
    setting.highwayMiddleZoneTags, setting.highwayMiddleZoneTagsCount,
    (char*)"middlew");

  char * request = new char[strlen(startZone)+strlen(endZone)+strlen(middleZone)+512];
  sprintf(
    request,
    "%s\n%s\n%s\nnode(w.startw)->.startn;node(w.endw)->.endn;node(w.middlew)->.middlen;(.startn; .startw; .endn; .endw; .middlen; .middlew;);out;",
    startZone, endZone, middleZone);

  delete startZone;
  delete endZone;
  delete middleZone;
  
  return request;
}

char * requeteRoute(
  double latStart, double lonStart,
  double latEnd, double lonEnd) {

  double latMiddle = geo_compute_middle_lat(latStart, latEnd);
  double lonMiddle = geo_compute_middle_lon(lonStart, lonEnd);

  double distance = geo_compute_cartesian_distance(latStart, lonStart, latEnd, lonEnd);

  double startRatio;
  double middleRatio;
  double endRatio;
  double seilDistance = 10000.0;
  double seilMiddleRation = 8.0/10.0;
  if(distance >= seilDistance) {
    middleRatio=seilMiddleRation;
  }
  else {
    middleRatio=(0.8/seilDistance)*distance;
  }
  startRatio=(1.0-middleRatio)/2 + 0.1;
  endRatio=(1.0-middleRatio)/2 + 0.1;

  double radiusStart = distance*startRatio;
  double radiusEnd = distance*endRatio;
  double radiusMiddle = distance*middleRatio;

  HighwayTag highwayStartEndZoneTags[] = {
    SECONDARY,
    TERTIARY,
    ROAD,
    RESIDENTIAL,
    UNCLASSIFIED,

    SECONDARY_LINK,
    TERTIARY_LINK
  };
  unsigned highwayStartEndZoneTagsCount = 7;
  HighwayTag highwayMiddleZoneTags[] = {
    MOTORWAY,
    TRUNK,
    PRIMARY,
    SECONDARY,
    ROAD,

    MOTORWAY_LINK,
    TRUNK_LINK,
    PRIMARY_LINK,
    SECONDARY_LINK,
  };
  unsigned highwayMiddleZoneTagsCount = 9;

  OverpassRequestSetting setting = {
    latStart,// latStart;
    lonStart,// lonStart;
    radiusStart,// radiusStart;
    
    latMiddle,// latMiddle;
    lonMiddle,// lonMiddle;
    radiusMiddle,// radiusMiddle;

    latEnd,// latEnd;
    lonEnd,// lonEnd;
    radiusEnd,// radiusEnd;


    highwayStartEndZoneTags,// HighwayTag * highwayStartZoneTags;
    highwayStartEndZoneTagsCount,// unsigned highwayStartZoneTagsCount;
    
    highwayMiddleZoneTags,// HighwayTag * highwayMiddleZoneTags;
    highwayMiddleZoneTagsCount,// unsigned highwayMiddleZoneTagsCount;
    
    highwayStartEndZoneTags,// HighwayTag * highwayEndZoneTags;
    highwayStartEndZoneTagsCount,// unsigned highwayEndZoneTagsCount;
  };

  return overpass_get_request_route(setting);
}



char * overpass_get_request_route_with_multi_zones(
  OverpassZoneRequestSetting * zonesSettings, unsigned zoneCount) {

  char * zonesRequests[zoneCount] = {};
  char * zonesNames[zoneCount] = {};

  for(unsigned i=0; i<zoneCount; i++) {
    zonesNames[i] = new char[128];
    memset(zonesNames[i], 0, 128);
    sprintf(zonesNames[i], "zone%d", i);

    OverpassZoneRequestSetting setting = zonesSettings[i];
    zonesRequests[i] = overpass_get_ways_tags_aggreg_block(
      setting.lat, setting.lon, setting.radius,
      setting.highwayTags, setting.highwayTagsCount,
      zonesNames[i]);
  }

  unsigned sizeRequestPartForWays = 0;
  unsigned sizeRequestPartForNodes = 0;
  unsigned sizeRequestPartForPack = 0;
  for(unsigned i=0; i<zoneCount; i++) {
    sizeRequestPartForWays += strlen(zonesRequests[i]);
    sizeRequestPartForNodes += strlen("node(w.)->.;") + 2*strlen(zonesNames[i]); // node(w.zone2)->.nzone2;
    sizeRequestPartForPack += 2*(5+strlen(zonesNames[i])); // .nzone2; .zone2;
  }
  sizeRequestPartForWays += 512; // For safety
  sizeRequestPartForNodes += 512; // For safety
  sizeRequestPartForPack += 512; // For safety

  char * requestPartForWays = new char[sizeRequestPartForWays];
  memset(requestPartForWays, 0, sizeRequestPartForWays);
  char * requestPartForNodes = new char[sizeRequestPartForNodes];
  memset(requestPartForNodes, 0, sizeRequestPartForNodes);
  char * requestPartForPack = new char[sizeRequestPartForPack];
  memset(requestPartForPack, 0, sizeRequestPartForPack);
  for(unsigned i=0; i<zoneCount; i++) {
    sprintf(requestPartForWays+strlen(requestPartForWays), "%s\n", zonesRequests[i]);
    sprintf(requestPartForNodes+strlen(requestPartForNodes), "node(w.%s)->.n%s; ", zonesNames[i], zonesNames[i]);
    sprintf(requestPartForPack+strlen(requestPartForPack), ".n%s; .%s;", zonesNames[i], zonesNames[i]);
  }

  unsigned sizeRequest = sizeRequestPartForWays+sizeRequestPartForNodes+sizeRequestPartForPack;
  sizeRequest += strlen("();out;\n");
  char * request = new char[sizeRequest];
  memset(request, 0, sizeRequest);
  sprintf(request, "%s\n %s\n (%s);out;\n", requestPartForWays, requestPartForNodes, requestPartForPack);

  for(unsigned i=0; i<zoneCount; i++) {
    delete zonesRequests[i];
    delete zonesNames[i];
  }
  delete requestPartForWays;
  delete requestPartForNodes;
  delete requestPartForPack;
  
  return request;
}

char * requeteRouteMultiZones(
  double latStart, double lonStart,
  double latEnd, double lonEnd) {

  double distance = geo_compute_cartesian_distance(latStart, lonStart, latEnd, lonEnd);
  unsigned middleZoneCount = 10;// (int) (distance/2400); // 2.4 Km

  // zones Ratios
  double startZoneRatio = 2.0/10.0;
  double endZoneRatio = 2.0/10.0;
  double midZoneRatio = (8.3/10.0) / middleZoneCount;

  // zones Tags
  unsigned highwayMiddleZoneTagsMiniCount = 6;
  HighwayTag highwayMiddleZoneTagsMini[] =
  {
    SECONDARY,
    ROAD,

    MOTORWAY_LINK,
    TRUNK_LINK,
    PRIMARY_LINK,
    SECONDARY_LINK
  };
  unsigned highwayMiddleZoneTagsMediumCount = 5;
  HighwayTag highwayMiddleZoneTagsMedium[] =
  {
    PRIMARY,
    SECONDARY,
    ROAD,

    PRIMARY_LINK,
    SECONDARY_LINK
  };
  unsigned highwayMiddleZoneTagsLargeCount = 4;
  HighwayTag highwayMiddleZoneTagsLarge[] =
  {
    MOTORWAY,
    TRUNK,

    MOTORWAY_LINK,
    TRUNK_LINK
  };
  HighwayTag * highwayMiddleZoneTags;
  unsigned highwayMiddleZoneTagsCount;
  if(distance <= 5000) {
    highwayMiddleZoneTags = highwayMiddleZoneTagsMini;
    highwayMiddleZoneTagsCount = highwayMiddleZoneTagsMiniCount;
    std::cout << "Use mini zones" << std::endl;
  }
  else if (distance <= 20000) {
    highwayMiddleZoneTags = highwayMiddleZoneTagsMedium;
    highwayMiddleZoneTagsCount = highwayMiddleZoneTagsMediumCount;
    std::cout << "Use medium zones" << std::endl;
  }
  else {
    highwayMiddleZoneTags = highwayMiddleZoneTagsLarge;
    highwayMiddleZoneTagsCount = highwayMiddleZoneTagsLargeCount;
    std::cout << "Use large zones" << std::endl;
  }

  HighwayTag highwayStartEndZoneTags[] = {
    SECONDARY,
    TERTIARY,
    ROAD,
    RESIDENTIAL,
    UNCLASSIFIED,

    SECONDARY_LINK,
    TERTIARY_LINK
  };
  unsigned highwayStartEndZoneTagsCount = 7;

  // request settings
  unsigned zoneCount = 2+middleZoneCount;
  OverpassZoneRequestSetting zonesSettings[zoneCount];
  zonesSettings[0] = { // Start zone
      latStart, // double lat;
      lonStart,// double lon;
      distance*startZoneRatio, // double radius;
      highwayStartEndZoneTags,// HighwayTag * highwayTags;
      highwayStartEndZoneTagsCount// unsigned highwayTagsCount;
    };
  zonesSettings[1] = { // End zone
      latEnd, // double lat;
      lonEnd,// double lon;
      distance*endZoneRatio, // double radius;
      highwayStartEndZoneTags,// HighwayTag * highwayTags;
      highwayStartEndZoneTagsCount// unsigned highwayTagsCount;
    };
  for(unsigned i=2; i<zoneCount; i++) {
    zonesSettings[i] = {
      ((i-1)*(latEnd-latStart)/middleZoneCount) + latStart,// double lat;
      ((i-1)*(lonEnd-lonStart)/middleZoneCount) + lonStart,// double lon;
      distance*midZoneRatio, // double radius;
      highwayMiddleZoneTags,// HighwayTag * highwayTags;
      highwayMiddleZoneTagsCount// unsigned highwayTagsCount;
    };
  }

  return overpass_get_request_route_with_multi_zones(zonesSettings, zoneCount);
}
