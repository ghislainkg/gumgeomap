#ifndef _PATH_REQUEST_HANDLER_
#define _PATH_REQUEST_HANDLER_

#include "matrix-manager.h"
#include "../path-find/path-finder.h"
#include "../config-manager/config-manager.h"

namespace requestmanager {

enum PathSearchType {
    FASTEST,
    SHORTEST,
    CHEAPPEST
};

/**This class inputs are two location (lat, lon). A start location and an arrival location.
 * It create a thread that :
 * - get the map (from the a map handler) according to the  start and arrival location.
 * - run path finding algorithm on it.
 * - and return the resulting path from the starting location to the arrival location.
*/
class PathRequestHandler {
public:
    PathRequestHandler(
        MatrixManagerRequestSettings requestSettings,
        osm_parsing::Mutex * mutex,
        osm_parsing::Matrix * matrix)
    : requestSettings(requestSettings),
    mutex(mutex), matrix(matrix) {}
    
    /**Update the matrix if necessary.*/
    bool performUpdateMatrix();
    /**Start pathfinding and return path.*/
    path_find::NodeResultList performPathFinding(
        bool * error,
        osm_parsing::MatrixNode * start,
        osm_parsing::MatrixNode * end);

    inline void setConfig(configmanager::ConfigManager * config) {
        this->config = config;
    }
    inline void setPathSearchType(PathSearchType pathSearchType) {
        this->pathSearchType = pathSearchType;
    }

private:
    MatrixManagerRequestSettings requestSettings;
    osm_parsing::Mutex * mutex;
    osm_parsing::Matrix * matrix;

    osm_parsing::MatrixNode * startNode;
    osm_parsing::MatrixNode * arrivalNode;
    PathSearchType pathSearchType = PathSearchType::SHORTEST;

    configmanager::ConfigManager * config;
};

}

#endif
