#include "path-request-handler.h"

namespace requestmanager {

bool PathRequestHandler::performUpdateMatrix() {
    if(matrix == nullptr) {
        std::cerr << "PathRequestHandler : Matrix is null" << std::endl;
        return false;
    }
    if(mutex == nullptr) {
        std::cerr << "PathRequestHandler : Mutex is null" << std::endl;
        return false;
    }

    this->matrix->setNullDistanceApproximation(config->nullDistanceApproximation());

    MatrixManager matrixManager = MatrixManager(this->matrix, this->mutex);
    matrixManager.setCurrentRequestSettings(&this->requestSettings);

    matrixManager.setLookSettingsNodeAmongInWayNode(config->LookSettingsNodeAmongInWayNode());
    matrixManager.setLookSettingsNodeAmongWayElementNode(config->LookSettingsNodeAmongWayElementNode());
    matrixManager.setMaxDistanceForSettingsNodeSearch(config->MaxDistanceForSettingsNodeSearch());
    matrixManager.setDefaultWaySpeed(config->defaultWaySpeed());
    matrixManager.setThreadCount(config->matrixBuildingThreadCount());

    bool result = matrixManager.perform(); // This makes the matrix grow if necessary.
    if(result) {
        this->startNode = matrixManager.getMatrixNodeForStart();
        this->arrivalNode = matrixManager.getMatrixNodeForArrival();
        return true;
    }
    else {
        std::cerr << "PathRequestHandler : Could not have the matrix to fit request start and arrival" << std::endl;
        return false;
    }
}

static value_types::Distance computeWeightForFastestPath(
    value_types::Distance speed,
    value_types::Distance distance,
    value_types::Distance price) {
    return (distance+price+1) * (speed+1);
}
static value_types::Distance computeWeightForShortestPath(
    value_types::Distance speed,
    value_types::Distance distance,
    value_types::Distance price) {
    return (speed+price+1) * (distance+1);
}
static value_types::Distance computeWeightForCheapestPath(
    value_types::Distance speed,
    value_types::Distance distance,
    value_types::Distance price) {
    return (distance+speed+1) * (price+1);
}

path_find::NodeResultList PathRequestHandler::performPathFinding(
    bool * error,
    osm_parsing::MatrixNode * start,
    osm_parsing::MatrixNode * end) {
    *error = false;
    if(startNode == nullptr || arrivalNode == nullptr) {
        std::cerr << "PathRequestHandler : Start or Arrival node are null" << std::endl;
        *error = true;
        return {};
    }
    if(matrix == nullptr) {
        std::cerr << "PathRequestHandler : Matrix is null for path finding" << std::endl;
        *error = true;
        return {};
    }
    
    path_find::PathFinder pathFinder = 
        path_find::PathFinder(matrix, this->startNode, this->arrivalNode);

    pathFinder.setOnlyWayElementAsExtraVoisin(config->WayElementAsExtraVoisin());
    pathFinder.setOnlyInWayAsExtraVoisin(config->InWayAsExtraVoisin());

    switch(this->pathSearchType) {
    case FASTEST:
        pathFinder.setArcComputeWeightFunction(computeWeightForFastestPath);
        break;
    case SHORTEST:
        pathFinder.setArcComputeWeightFunction(computeWeightForShortestPath);
        break;
    case CHEAPPEST:
        pathFinder.setArcComputeWeightFunction(computeWeightForCheapestPath);
        break;
    }

    *start = *(this->startNode);
    *end = *(this->arrivalNode);
    return pathFinder.perform();
}

}
