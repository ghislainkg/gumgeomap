#ifndef _PATH_REQUEST_MANAGER_
#define _PATH_REQUEST_MANAGER_

#include "path-request-handler.h"

#include <pthread.h>

namespace requestmanager {

typedef void (*OnPathResponseFunction)(
    std::vector<path_find::NodeResultList*> & pathList,
    osm_parsing::MatrixNode start,
    osm_parsing::MatrixNode end,
    osm_parsing::Matrix * matrix,
    bool useDisplay,
    void * data);
typedef void (*OnPathSearchFailFunction)(osm_parsing::Matrix * matrix, void * data);

class PathResquestManager {
public:
    PathResquestManager() {
        this->matrixMutex = new osm_parsing::Mutex();
    	this->matrix = new osm_parsing::Matrix(0);
    }

    void addRequest(
        MatrixManagerRequestSettings settings,
        OnPathResponseFunction onResponseFunction,
        OnPathSearchFailFunction onPathSearchFailFunction, void * data);

    inline osm_parsing::Mutex * getMatrixMutex() {
        return matrixMutex;
    }
    inline osm_parsing::Matrix * getMatrix() {
        return matrix;
    }

    inline void setConfig(configmanager::ConfigManager * config) {
        this->config = config;
        this->matrix->setAllocExtraSize(this->config->allocExtraSize());
        this->matrix->setOptionalMaxNodeCount(this->config->maxNodeCount());
    }
    inline configmanager::ConfigManager * getConfig() {
        return config;
    }

private:
    osm_parsing::Mutex * matrixMutex;
    osm_parsing::Matrix * matrix;

    configmanager::ConfigManager * config;
};

}

#endif
