#include "path-request-manager.h"

namespace requestmanager {

static void threadFunction(void ** args)
{
    MatrixManagerRequestSettings * settings = (MatrixManagerRequestSettings*) args[0];
    osm_parsing::Matrix * matrix = (osm_parsing::Matrix*) args[1];
    osm_parsing::Mutex * matrixMutex = (osm_parsing::Mutex *) args[2];
    OnPathResponseFunction * onResponseFunction = (OnPathResponseFunction*) args[3];
    OnPathSearchFailFunction * onPathSearchFailFunction = (OnPathSearchFailFunction*) args[4];
    void * data = args[5];
    configmanager::ConfigManager * config = (configmanager::ConfigManager *) args[6];

    std::cout << "Matrix = " << matrix->getSize() << std::endl;

    PathRequestHandler requestHandler =
		PathRequestHandler(*settings, matrixMutex, matrix);
    requestHandler.setConfig(config);
    
    bool result = requestHandler.performUpdateMatrix();

    if(result) {
        bool error;
        osm_parsing::MatrixNode start;
        osm_parsing::MatrixNode end;

        std::vector<path_find::NodeResultList*> pathList;

        requestHandler.setPathSearchType(PathSearchType::FASTEST);
        auto fastPath = requestHandler.performPathFinding(&error, &start, &end);
        if(error || fastPath.size() <= 2) {
            std::cerr << "PathResquestManager : Error while performing path finding" << std::endl;
        }
        else {
            pathList.push_back(&fastPath);
        }

        requestHandler.setPathSearchType(PathSearchType::SHORTEST);
        auto shortPath = requestHandler.performPathFinding(&error, &start, &end);
        if(error || shortPath.size() <= 2) {
            std::cerr << "PathResquestManager : Error while performing path finding" << std::endl;
        }
        else {
            pathList.push_back(&shortPath);
        }

        requestHandler.setPathSearchType(PathSearchType::CHEAPPEST);
        auto cheapPath = requestHandler.performPathFinding(&error, &start, &end);
        if(error || cheapPath.size() <= 2) {
            std::cerr << "PathResquestManager : Error while performing path finding" << std::endl;
        }
        else {
            pathList.push_back(&cheapPath);
        }

        if(pathList.size() == 0) {
            (*onPathSearchFailFunction)(matrix, data);
        }
        else {
            (*onResponseFunction)(pathList, start, end, matrix, config->useDisplay(), data);
        }
    }
    else {
        std::cerr << "PathResquestManager : Error while performing matrix update" << std::endl;
        (*onPathSearchFailFunction)(matrix, data);
    }

    delete settings;
}

void PathResquestManager::addRequest(
    MatrixManagerRequestSettings settings,
    OnPathResponseFunction onResponseFunction,
    OnPathSearchFailFunction onPathSearchFailFunction, void * data)
{
    if(matrix == nullptr) {
        std::cerr << "PathResquestManager : Matrix is null" << std::endl;
        return;
    }
    if(matrixMutex == nullptr) {
        std::cerr << "PathResquestManager : Matrix mutex is null" << std::endl;
        return;
    }
    if(onResponseFunction == nullptr) {
        std::cerr << "PathResquestManager : Response function is null" << std::endl;
        return;
    }
    MatrixManagerRequestSettings * se = new MatrixManagerRequestSettings();
    se->startID = settings.startID;
    se->arrivalID = settings.arrivalID;
    se->startLongitude = settings.startLongitude;
    se->startLatitude = settings.startLatitude;
    se->arrivalLongitude = settings.arrivalLongitude;
    se->arrivalLatitude = settings.arrivalLatitude;

    void ** args = new void*[7];
    args[0] = se;
    args[1] = matrix;
    args[2] = matrixMutex;
    args[3] = &onResponseFunction;
    args[4] = &onPathSearchFailFunction;
    args[5] = data;
    args[6] = config;
    // threadFunction(args);
    pthread_t thread;
    pthread_create(
        &thread, nullptr,
        (void*(*)(void*))threadFunction, args);
}

}
