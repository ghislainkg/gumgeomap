#include "matrix-manager.h"

namespace requestmanager {

bool MatrixManager::perform() {
    currentStartNode = nullptr;
    currentArrivalNode = nullptr;
    bool result = this->getRequestedNodes(&currentStartNode, &currentArrivalNode);
    if(!result) {
        result = queryMapForSettings();
        if(result) {
            this->matrixBuilder->setMatrix(this->matrix);
            this->matrixBuilder->buildInCurrentMatrix(this->nodeCountInXml);
            delete this->matrixBuilder;
            this->currentStartNode = this->matrix->getNodeCloseTo(
                this->currentRequestSettings->startLatitude,
                this->currentRequestSettings->startLongitude,
                this->lookSettingsNodeAmongWayElementNode,
                this->lookSettingsNodeAmongInWayNode,
                this->maxDistanceForSettingsNodeSearch
            );
            if(currentStartNode == nullptr) {
                std::cerr << "MatrixManager : No start node found in matrix at provided location" << std::endl;
                return false;
            }
            this->currentArrivalNode = this->matrix->getNodeCloseTo(
                this->currentRequestSettings->arrivalLatitude,
                this->currentRequestSettings->arrivalLongitude,
                this->lookSettingsNodeAmongWayElementNode,
                this->lookSettingsNodeAmongInWayNode,
                this->maxDistanceForSettingsNodeSearch
            );
            if(currentArrivalNode == nullptr) {
                std::cerr << "MatrixManager : No arrival node found in matrix at provided location" << std::endl;
                return false;
            }
        }
        else {
            return false;
        }
    }
    return true;
}

bool MatrixManager::getRequestedNodes(
    osm_parsing::MatrixNode ** startNode, osm_parsing::MatrixNode ** arrivalNode
) {
    auto sNode = this->matrix->getNodeById(this->currentRequestSettings->startID);
    if(sNode == nullptr) {
        return false;
    }
    auto aNode = this->matrix->getNodeById(this->currentRequestSettings->arrivalID);
    if(aNode == nullptr) {
        return false;
    }
    *startNode = sNode;
    *arrivalNode = aNode;
    return true;
}

bool MatrixManager::queryMapForSettings() {
    auto response = osm_request::performRequest(
        currentRequestSettings->startLatitude,
        currentRequestSettings->startLongitude,
        currentRequestSettings->arrivalLatitude,
        currentRequestSettings->arrivalLongitude
    );
    if(osm_request::isOsmResponseOk(response)) {
        this->nodeCountInXml = osm_parsing::countNodeInXmlString(response.c_str(), response.size());
        this->matrixBuilder = new osm_parsing::MatrixBuilder(response.c_str());

        this->matrixBuilder->setDefaultWaySpeed(this->defaultWaySpeed);
        this->matrixBuilder->setThreadCount(this->threadCount);

        this->matrixBuilder->setCustomMutexForMatrixWritingBlock(this->mutex);
        return true;
    }
    else {
        std::cerr << "MatrixManager : Bad response for Osm request" << std::endl;
        std::cerr << response << std::endl; 
    }

    return false;
}

}