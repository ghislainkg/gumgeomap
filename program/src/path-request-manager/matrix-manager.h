#ifndef _MATRIX_MANAGER_
#define _MATRIX_MANAGER_

#include "../osm-xml-parsing/osm-matrix-builder.h"
#include "../osm-request/osm-request.h"

namespace requestmanager {

typedef struct {
    /**Id (osm id) of the starting node.
     * Is used to check if the node is already in the matrix.*/
    value_types::Id startID;
    /**Id (osm id) of the arriavl node.
     * Is used to check if the node is already in the matrix.*/
    value_types::Id arrivalID;

    value_types::Distance startLongitude;
    value_types::Distance startLatitude;
    value_types::Distance arrivalLongitude;
    value_types::Distance arrivalLatitude;
} MatrixManagerRequestSettings;

/**This class manage the global map.
 * - TODO : It get request of locations (start and arrival), if a location is not in the matrix,
 * the class perform the necessary request to openstreetmap to get the map zone containing these locations.
 * - TODO : It also implement a timer for chunks of the map added when requesting new locations.
 * Once the timer is over, the chunks of maps are removed.
 * - TODO : It can also remove chunks if the authorised memory size for the map is reached.
*/
class MatrixManager {
public:
    /**Initialise the matrix builder and the matrix.*/
    MatrixManager(osm_parsing::Matrix * matrix, osm_parsing::Mutex * mutex)
    : matrixBuilder(nullptr), matrix(matrix), mutex(mutex),
    currentRequestSettings(nullptr), currentStartNode(nullptr), currentArrivalNode(nullptr)
    {}
    ~MatrixManager() {}

    inline void setCurrentRequestSettings(const MatrixManagerRequestSettings * currentRequestSettings)
        {this->currentStartNode = nullptr;
        this->currentArrivalNode = nullptr;
        this->currentRequestSettings = currentRequestSettings;}

    inline void setMutexForMatrixWritingBlock(osm_parsing::Mutex * mutex)
        { this->mutex = mutex; }

    inline void setMatrix(osm_parsing::Matrix * matrix) 
        { this->matrix = matrix; }

    /**Note : Memory managed by the class.*/
    inline osm_parsing::MatrixNode * getMatrixNodeForStart() const
        {return currentStartNode;}
    /**Note : Memory managed by the class.*/
    inline osm_parsing::MatrixNode * getMatrixNodeForArrival() const
        {return currentArrivalNode;}

    inline void setLookSettingsNodeAmongInWayNode(bool value) {
        lookSettingsNodeAmongInWayNode = value;
    }
    inline void setLookSettingsNodeAmongWayElementNode(bool value) {
        lookSettingsNodeAmongWayElementNode = value;
    }
    inline void setMaxDistanceForSettingsNodeSearch(value_types::Distance value) {
        maxDistanceForSettingsNodeSearch = value;
    }
    inline void setThreadCount(unsigned value) {
        threadCount = value;
    }
    inline void setDefaultWaySpeed(value_types::Distance value) {
        defaultWaySpeed = value;
    }

    /**Perform necesarry actions to provide to the class a matrix containing
     * the start and arrival from the request settings*/
    bool perform();

private:
    osm_parsing::MatrixBuilder * matrixBuilder;
    osm_parsing::Matrix * matrix;
    osm_parsing::Mutex * mutex;

    const MatrixManagerRequestSettings * currentRequestSettings;
    osm_parsing::MatrixNode * currentStartNode;
    osm_parsing::MatrixNode * currentArrivalNode;

    unsigned long nodeCountInXml;

    bool lookSettingsNodeAmongInWayNode = true;
    bool lookSettingsNodeAmongWayElementNode = true;
    value_types::Distance maxDistanceForSettingsNodeSearch = 100.0; // 100 metres
    unsigned threadCount = 1;
    value_types::Distance defaultWaySpeed;

    /**Return true and the node with the ids specified in the request settings if they exist in the matrix.
     * Return false if they don't exist in the matrix.*/
    bool getRequestedNodes(osm_parsing::MatrixNode ** startNode, osm_parsing::MatrixNode ** arrivalNode);

    /**Makes a query to osm to get a map zone containing node
     * with the arrival and start latitude and longitude.
     * Then setup the matrix builder with the osm response.
     * If the response is not a map, return false.*/
    bool queryMapForSettings();
};

}

#endif
