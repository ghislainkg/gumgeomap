#include "osm-request/osm-request.h"
#include "osm-xml-parsing/osm-matrix-builder.h"
#include "path-find/path-finder.h"

#include "osm-request/request.h"

#include <algorithm>

void showVoisins(std::vector<osm_parsing::Voisin> * voisins, value_types::Index limit = value_types::MAX_INDEX) { 
	for(value_types::Index i=0; i< voisins->size() && i < limit; i++) {
		osm_parsing::Voisin & voisin = voisins->at(i);
		std::cout << "\t" << voisin.voisin->getId() << std::endl;
	}
}
void showVoisinInMatrix(osm_parsing::Matrix * matrix) {
	for(value_types::Index i=0; i< matrix->getSize(); i++) {
		auto node = matrix->getNode(i);
		auto voisins = matrix->getVoisins(node);
		std::cout << i << " Node : " << node->getId() << " has " << voisins->size() << " neighbors " << std::endl;
		showVoisins(voisins, 100);
	}
}
void showNodesInMatrix(osm_parsing::Matrix * matrix) {
	for(value_types::Index i=0; i< matrix->getSize(); i++) {
		auto node = matrix->getNode(i);
		std::cout << i << " Node : " << node->getId() << " lat=" << node->getLatitude() << " lon=" << node->getLongitude() << std::endl;
	}
}

void showPath(std::vector<path_find::Node> & path) {
	std::cout << "Path (size=" << path.size() << ")" << std::endl;
	for(value_types::Index i=path.size()-1; i>=0; i--) {
		osm_parsing::MatrixNode * n = path.at(i).getMatrixNode();
		std::cout << " Node " << n->getId() << std::endl;
		if(i==0) {
			break;
		}
	}
}

value_types::Distance getMinLatitude(osm_parsing::Matrix * matrix) {
	if(matrix->getSize() < 1)
		return 0;
	value_types::Distance min = matrix->getNode(0)->getLatitude();
	for(value_types::Index i=1; i<matrix->getSize(); i++) {
		value_types::Distance lat = matrix->getNode(i)->getLatitude();
		if(lat < min) {
			min = lat;
		}
	}
	return min;
}

value_types::Distance getMaxLatitude(osm_parsing::Matrix * matrix) {
	if(matrix->getSize() < 1)
		return 0;
	value_types::Distance max = matrix->getNode(0)->getLatitude();
	for(value_types::Index i=1; i<matrix->getSize(); i++) {
		value_types::Distance lat = matrix->getNode(i)->getLatitude();
		if(lat > max) {
			max = lat;
		}
	}
	return max;
}

value_types::Distance getMinLongitude(osm_parsing::Matrix * matrix) {
	if(matrix->getSize() < 1)
		return 0;
	value_types::Distance min = matrix->getNode(0)->getLongitude();
	for(value_types::Index i=1; i<matrix->getSize(); i++) {
		value_types::Distance lat = matrix->getNode(i)->getLongitude();
		if(lat < min) {
			min = lat;
		}
	}
	return min;
}

value_types::Distance getMaxLongitude(osm_parsing::Matrix * matrix) {
	if(matrix->getSize() < 1)
		return 0;
	value_types::Distance max = matrix->getNode(0)->getLongitude();
	for(value_types::Index i=1; i<matrix->getSize(); i++) {
		value_types::Distance lat = matrix->getNode(i)->getLongitude();
		if(lat > max) {
			max = lat;
		}
	}
	return max;
}

std::vector<unsigned> sortPathByScore(std::vector<unsigned> & scoreList) {
	std::vector<unsigned> indices;
	indices.reserve(scoreList.size());
	for(unsigned i=0; i<scoreList.size(); i++) {
		indices.push_back(scoreList[i]);
	}

	std::sort(indices.begin(), indices.end());

	for(unsigned i=0; i<scoreList.size(); i++) {
		indices[i] = i;
	}

	return indices;
}
