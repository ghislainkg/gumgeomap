#include "config-manager.h"

namespace configmanager {

ConfigManager::ConfigManager(const char * configFilename) {
	this->configFilename = new char[strlen(configFilename)];
	memcpy(this->configFilename, configFilename, strlen(configFilename));
}

std::string ConfigManager::getConfigStr() {
	bool success;
	std::string res = filehandling::readFile(configFilename, success);
	if(!success) {
		std::cerr << "Could'n access config file" << std::endl;
		return "";
	}
	return res;
}

void ConfigManager::setConfigStr(std::string & content) {
	bool success;
	filehandling::writeFile(configFilename, content, success);
	if(!success) {
		std::cerr << "Could'n update config file" << std::endl;
	}
}

rapidjson::Document ConfigManager::getConfigJsonDoc(bool & success) {
	std::string json = getConfigStr();
	if(json.empty()) {
		success = false;
	}

	rapidjson::Document doc;
	doc.Parse(json.data());

	if(!doc.IsObject()) {
		std::cout << json << std::endl;
		exit(1);
		std::cout << "Invalid config file: " << configFilename << std::endl;
		success = false;
		return doc;
	}
	success = true;
	return doc;
}

#define SET_CONFIG_JSON_PARAM(key, value) \
	bool success; \
	auto doc = getConfigJsonDoc(success); \
	// if(!success) return 8080; \
	rapidjson::Value VALUE; \
	VALUE = value; \
	doc.AddMember(key, VALUE, doc.GetAllocator()); \
	std::string json; \
	rapidjson::StringBuffer buffer; \
	buffer.Clear(); \
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer); \
	doc.Accept(writer); \
	json.insert(0, strdup(buffer.GetString())); \
	setConfigStr(json);


int ConfigManager::inputsPortNumber() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return 8080;

	if(doc.HasMember("inputsPortNumber") && doc["inputsPortNumber"].IsInt()) {
		return doc["inputsPortNumber"].GetInt();
	}
	else {
		std::cout << "Invalid config file (inputsPortNumber): " << configFilename << std::endl;
		return 8080;
	}
}
const char * ConfigManager::scoreServerAddress() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return nullptr;

	if(doc.HasMember("scoreServerAddress") && doc["scoreServerAddress"].IsString()) {
		return doc["scoreServerAddress"].GetString();
	}
	else {
		std::cout << "Invalid config file (scoreServerAddress): " << configFilename << std::endl;
		char * defaultAddress = new char[256];
		sprintf(defaultAddress, "locahost");
		return defaultAddress;
	}
}
int ConfigManager::scoreServerPort() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return 8081;

	if(doc.HasMember("scoreServerPort") && doc["scoreServerPort"].IsInt()) {
		return doc["scoreServerPort"].GetInt();
	}
	else {
		std::cout << "Invalid config file (scoreServerPort): " << configFilename << std::endl;
		return 8081;
	}
}
const char * ConfigManager::overpassUrl() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return nullptr;

	if(doc.HasMember("overpassUrl") && doc["overpassUrl"].IsString()) {
		return doc["overpassUrl"].GetString();
	}
	else {
		std::cout << "Invalid config file (overpassUrl): " << configFilename << std::endl;
		char * defaultAddress = new char[256];
		sprintf(defaultAddress, "http://overpass.openstreetmap.fr/api/interpreter");
		return defaultAddress;
	}
}
bool ConfigManager::useDisplay() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return false;

	if(doc.HasMember("useDisplay") && doc["useDisplay"].IsBool()) {
		return doc["useDisplay"].GetBool();
	}
	else {
		std::cout << "Invalid config file (useDisplay): " << configFilename << std::endl;
		return false;
	}
}
value_types::Distance ConfigManager::defaultWaySpeed() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return 50.0;

	if(doc.HasMember("defaultWaySpeed") && doc["defaultWaySpeed"].IsDouble()) {
		return doc["defaultWaySpeed"].GetDouble();
	}
	else {
		std::cout << "Invalid config file (defaultWaySpeed): " << configFilename << std::endl;
		return 50.0;
	}
}
int ConfigManager::maxNodeCount() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return std::numeric_limits<int>::max();

	if(doc.HasMember("scoreServerPort") && doc["scoreServerPort"].IsInt()) {
		int res = doc["scoreServerPort"].GetInt();
		if(res < 0) {
			return std::numeric_limits<int>::max();
		}
		else {
			return res;
		}
	}
	else {
		std::cout << "Invalid config file (scoreServerPort): " << configFilename << std::endl;
		return std::numeric_limits<int>::max();
	}
}
int ConfigManager::allocExtraSize() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return 10000;

	if(doc.HasMember("allocExtraSize") && doc["allocExtraSize"].IsInt()) {
		return doc["allocExtraSize"].GetInt();
	}
	else {
		std::cout << "Invalid config file (allocExtraSize): " << configFilename << std::endl;
		return 10000;
	}
}
value_types::Distance ConfigManager::nullDistanceApproximation() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return 10.0;

	if(doc.HasMember("nullDistanceApproximation") && doc["nullDistanceApproximation"].IsDouble()) {
		return doc["nullDistanceApproximation"].GetDouble();
	}
	else {
		std::cout << "Invalid config file (nullDistanceApproximation): " << configFilename << std::endl;
		return 10.0;
	}
}
int ConfigManager::matrixBuildingThreadCount() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return 8081;

	if(doc.HasMember("matrixBuildingThreadCount") && doc["matrixBuildingThreadCount"].IsInt()) {
		return doc["matrixBuildingThreadCount"].GetInt();
	}
	else {
		std::cout << "Invalid config file (matrixBuildingThreadCount): " << configFilename << std::endl;
		return 8081;
	}
}
bool ConfigManager::LookSettingsNodeAmongInWayNode() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return false;

	if(doc.HasMember("LookSettingsNodeAmongInWayNode") && doc["LookSettingsNodeAmongInWayNode"].IsBool()) {
		return doc["LookSettingsNodeAmongInWayNode"].GetBool();
	}
	else {
		std::cout << "Invalid config file (LookSettingsNodeAmongInWayNode): " << configFilename << std::endl;
		return false;
	}
}
bool ConfigManager::LookSettingsNodeAmongWayElementNode() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return false;

	if(doc.HasMember("LookSettingsNodeAmongWayElementNode") && doc["LookSettingsNodeAmongWayElementNode"].IsBool()) {
		return doc["LookSettingsNodeAmongWayElementNode"].GetBool();
	}
	else {
		std::cout << "Invalid config file (LookSettingsNodeAmongWayElementNode): " << configFilename << std::endl;
		return false;
	}
}
double ConfigManager::MaxDistanceForSettingsNodeSearch() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return 10.0;

	if(doc.HasMember("MaxDistanceForSettingsNodeSearch") && doc["MaxDistanceForSettingsNodeSearch"].IsDouble()) {
		return doc["MaxDistanceForSettingsNodeSearch"].GetDouble();
	}
	else {
		std::cout << "Invalid config file (MaxDistanceForSettingsNodeSearch): " << configFilename << std::endl;
		return 10.0;
	}
}
bool ConfigManager::WayElementAsExtraVoisin() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return false;

	if(doc.HasMember("WayElementAsExtraVoisin") && doc["WayElementAsExtraVoisin"].IsBool()) {
		return doc["WayElementAsExtraVoisin"].GetBool();
	}
	else {
		std::cout << "Invalid config file (WayElementAsExtraVoisin): " << configFilename << std::endl;
		return false;
	}
}
bool ConfigManager::InWayAsExtraVoisin() {
	bool success;
	auto doc = getConfigJsonDoc(success);
	if(!success) return false;

	if(doc.HasMember("InWayAsExtraVoisin") && doc["InWayAsExtraVoisin"].IsBool()) {
		return doc["InWayAsExtraVoisin"].GetBool();
	}
	else {
		std::cout << "Invalid config file (useDInWayAsExtraVoisinisplay): " << configFilename << std::endl;
		return false;
	}
}


void ConfigManager::setinputsPortNumber(int value) {
	SET_CONFIG_JSON_PARAM("inputsPortNumber", value);
}
void ConfigManager::setscoreServerAddress(const char * value) {
	SET_CONFIG_JSON_PARAM("scoreServerAddress", value);
}
void ConfigManager::setscoreServerPort(int value) {
	SET_CONFIG_JSON_PARAM("scoreServerPort", value);
}
void ConfigManager::setoverpassUrl(const char * value) {
	SET_CONFIG_JSON_PARAM("overpassUrl", value);
}
void ConfigManager::setuseDisplay(bool value) {
	SET_CONFIG_JSON_PARAM("useDisplay", value);
}
void ConfigManager::setdefaultWaySpeed(value_types::Distance value) {
	SET_CONFIG_JSON_PARAM("defaultWaySpeed", value);
}
void ConfigManager::setmaxNodeCount(value_types::Distance value) {
	SET_CONFIG_JSON_PARAM("maxNodeCount", value);
}
void ConfigManager::setallocExtraSize(int value) {
	SET_CONFIG_JSON_PARAM("allocExtraSize", value);
}
void ConfigManager::setnullDistanceApproximation(value_types::Distance value) {
	SET_CONFIG_JSON_PARAM("nullDistanceApproximation", value);
}
void ConfigManager::setmatrixBuildingThreadCount(int value) {
	SET_CONFIG_JSON_PARAM("matrixBuildingThreadCount", value);
}
void ConfigManager::setLookSettingsNodeAmongInWayNode(bool value) {
	SET_CONFIG_JSON_PARAM("LookSettingsNodeAmongInWayNode", value);
}
void ConfigManager::setLookSettingsNodeAmongWayElementNode(bool value) {
	SET_CONFIG_JSON_PARAM("LookSettingsNodeAmongWayElementNode", value);
}
void ConfigManager::setMaxDistanceForSettingsNodeSearch(bool value) {
	SET_CONFIG_JSON_PARAM("MaxDistanceForSettingsNodeSearch", value);
}
void ConfigManager::setWayElementAsExtraVoisin(bool value) {
	SET_CONFIG_JSON_PARAM("WayElementAsExtraVoisin", value);
}
void ConfigManager::setInWayAsExtraVoisin(bool value) {
	SET_CONFIG_JSON_PARAM("InWayAsExtraVoisin", value);
}

}
