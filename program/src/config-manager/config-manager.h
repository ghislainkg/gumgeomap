#ifndef _CONFIG_MANAGER_
#define _CONFIG_MANAGER_

#include "../utils/value-types.h"
#include "../utils/files.h"

#include "../vendors/rapidjson/document.h"
#include "../vendors/rapidjson/writer.h"
#include "../vendors/rapidjson/stringbuffer.h"

#include <cstdlib>
#include <cstring>

namespace configmanager {

class ConfigManager {
public:
	ConfigManager(const char * configFilename);

	int inputsPortNumber();
	const char * scoreServerAddress();
	int scoreServerPort();
	const char * overpassUrl();
	bool useDisplay();
	value_types::Distance defaultWaySpeed();
	int maxNodeCount();
	int allocExtraSize();
	value_types::Distance nullDistanceApproximation();
	int matrixBuildingThreadCount();
	bool LookSettingsNodeAmongInWayNode();
	bool LookSettingsNodeAmongWayElementNode();
	double MaxDistanceForSettingsNodeSearch();
	bool WayElementAsExtraVoisin();
	bool InWayAsExtraVoisin();

	void setinputsPortNumber(int value);
	void setscoreServerAddress(const char * value);
	void setscoreServerPort(int value);
	void setoverpassUrl(const char * value);
	void setuseDisplay(bool value);
	void setdefaultWaySpeed(value_types::Distance value);
	void setmaxNodeCount(value_types::Distance value);
	void setallocExtraSize(int value);
	void setnullDistanceApproximation(value_types::Distance value);
	void setmatrixBuildingThreadCount(int value);
	void setLookSettingsNodeAmongInWayNode(bool value);
	void setLookSettingsNodeAmongWayElementNode(bool value);
	void setMaxDistanceForSettingsNodeSearch(bool value);
	void setWayElementAsExtraVoisin(bool value);
	void setInWayAsExtraVoisin(bool value);

private:
	char * configFilename;

	std::string getConfigStr();
	void setConfigStr(std::string & content);
	rapidjson::Document getConfigJsonDoc(bool & success);

};

}

#endif
