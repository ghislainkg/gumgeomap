#ifndef _FILE_HANDLING_
#define _FILE_HANDLING_

#include <fstream>
#include <iostream>

namespace filehandling {

inline std::string readFile(const char * filename, bool & success) {
	std::string res = "";
	std::string line;
	std::ifstream file(filename);
	if(file.is_open()) {
		while(std::getline(file, line)) {
			if(line.front() == '|')
				break;
			res += line+"\n";
		}
		file.close();
	}
	else {
		std::cerr << "Could'n read the file '" << filename << "'" << std::endl;
		success = false;
	}
	success = true;
	return res;
}

inline void writeFile(const char * filename, std::string & content, bool & success) {
	std::ofstream mfile;
	mfile.open(filename);
	if(mfile.is_open())  {
		mfile << content << std::endl;
		mfile.close();
	}
	else {
		std::cerr << "Could'n write in the file '" << filename << "'" << std::endl;
		success = false;
	}
	success = true;
}

}

#endif
