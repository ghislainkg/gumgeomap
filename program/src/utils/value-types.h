#ifndef _TYPES_
#define _TYPES_

#include <cstdio>
#include <cmath>
#include <limits>

namespace value_types {
    typedef long double Distance;
    typedef unsigned long Id;
    typedef unsigned long Index;

    static const Distance MAX_DISTANCE = std::numeric_limits<Distance>::max();
    static const Id MAX_ID = std::numeric_limits<Id>::max();
    static const Index MAX_INDEX = std::numeric_limits<Index>::max();
};


#define EART_RADIUS_METERS 6371000 
#define PI_APPROX 3.141592

inline double geo_compute_cartesian_coord(double geoAngle) {
  return (geoAngle*PI_APPROX/180.0)*EART_RADIUS_METERS;
}
inline double geo_compute_cartesian_distance(double lat1, double lon1, double lat2, double lon2) {
  float lat1Cart = geo_compute_cartesian_coord(lat1);
  float lon1Cart = geo_compute_cartesian_coord(lon1);
  float lat2Cart = geo_compute_cartesian_coord(lat2);
  float lon2Cart = geo_compute_cartesian_coord(lon2);
  return std::sqrt((lat1Cart-lat2Cart)*(lat1Cart-lat2Cart) + (lon1Cart-lon2Cart)*(lon1Cart-lon2Cart));
}
inline double geo_compute_middle_lat(double lat1, double lat2) {
  return (lat1+lat2)/2.0;
}
inline double geo_compute_middle_lon(double lon1, double lon2) {
  return (lon1+lon2)/2.0;
}

#endif
