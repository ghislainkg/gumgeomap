#include "display-map.h"

namespace display_map {

int mapToScreenX(value_types::Distance x, MapToScreenSpace & converter) {
    return converter.alphaX*x+converter.betaX;
}

int mapToScreenY(value_types::Distance y, MapToScreenSpace & converter) {
    return converter.alphaY*y+converter.betaY;
}

void DisplayMap::start(
        value_types::Distance limUp, value_types::Distance limDown,
        value_types::Distance limLeft, value_types::Distance limRight,
        value_types::Distance screenWidth, value_types::Distance screenHeight,
        value_types::Distance borderX,
        value_types::Distance borderY) {
    
    cimg_library::CImg<unsigned char> block(screenWidth+2*borderX, screenHeight+2*borderY, 1, 3, 0);

    MapToScreenSpace converter = {
        screenWidth/(limRight-limLeft),
        borderX-( screenWidth/(limRight-limLeft) )*limLeft,
        screenHeight/(limDown-limUp),
        borderY-( screenHeight/(limDown-limUp) )*limUp
    };

    std::cout << "Converter :"
		<< " alphaX=" << converter.alphaX 
		<< " alphaY=" << converter.alphaY
		<< " betaX=" << converter.betaX
		<< " betaY=" << converter.betaY
		<< std::endl;

    const unsigned char colorNode[] = {160, 2, 0};
    const unsigned char colorOtherNodes1[] = {2, 160, 0};
    const unsigned char colorOtherNodes2[] = {2, 0, 160};
    const unsigned char colorText[] = {255, 255, 255};
    const unsigned char colorArc[] = {255, 255, 0};
    if(matrix != nullptr) {
        for(value_types::Index i=0; i<this->matrix->getSize(); i++) {
            auto node = this->matrix->getNode(i);
            
            // std::cout
            //     << "Point : " << node->getLatitude() << ", "
            //     << node->getLongitude() << std::endl;
            
            int x = mapToScreenX(node->getLatitude(), converter);
            int y = mapToScreenY(node->getLongitude(), converter);
            
            //std::cout << "\t" << x << ", " << y << std::endl;

            block.draw_circle(x, y, 2, colorNode);
            // std::cout << "Id = " << node->getId() << std::endl;
            // std::cout << "Matrix "<<i<<": lat="<<node->getLatitude()<<" lon="<<node->getLongitude() << std::endl;
            // std::cout << "Matrix "<<i<<": x="<<x<<" y="<<y << std::endl;

            char * text = new char[5];
            sprintf(text, "%ld", i);
            block.draw_text(x, y, text, colorText, 0, 1.f, 10);

            auto voisins = this->matrix->getVoisins(node);
            for(osm_parsing::Voisin v : *voisins) {
                int xv = mapToScreenX(v.voisin->getLatitude(), converter);
                int yv = mapToScreenY(v.voisin->getLongitude(), converter);
                block.draw_line(x, y, xv, yv, colorArc);
            }
            delete voisins;
            delete text;
        }
    }
    else {
        std::cout << "DisplayMap : Matrix is null" << std::endl;
    }

    for(unsigned i=0; i<otherNodes.size(); i++) {
        auto node = otherNodes[i];

        int x = mapToScreenX(node.getLatitude(), converter);
        int y = mapToScreenY(node.getLongitude(), converter);

        std::cout << "Other Node "<<i<<": lat="<<node.getLatitude()<<" lon="<<node.getLongitude() << std::endl;
        
        if(i %2 == 0) {
            block.draw_circle(x, y, 8, colorOtherNodes1);
        }
        else {
            block.draw_circle(x, y, 10, colorOtherNodes2);
        }
    }

    std::cout << "Paths count : " << pathList.size() << std::endl;

    for(unsigned i=0; i<pathList.size(); i++) {
        auto path = pathList[i];
        unsigned char colorIndex = i;
        const unsigned char colorPath[] = {
            255-colorIndex%3*(255/3), 
            colorIndex%2*(255/2),
            colorIndex%4*(255/4)};
        int xprev = INT32_MAX;
        int yprev = INT32_MAX;
        int x;
        int y;
        for(value_types::Index i=0; i<path->size(); i++) {
            auto node = &(path->at(i));
            x = mapToScreenX(node->getMatrixNode()->getLatitude(), converter);
            y = mapToScreenY(node->getMatrixNode()->getLongitude(), converter);
            // std::cout << "Path Point After : x="<<x<<" y="<<y << std::endl;

            if(xprev != INT32_MAX && yprev != INT32_MAX) {
                block.draw_line(x, y, xprev, yprev, colorPath);
            }
            else {
                const unsigned char colorStart[] = {0, 0, 200};
                block.draw_circle(x, y, 4, colorStart);
            }

            xprev = x;
            yprev = y;
        }
        const unsigned char colorEnd[] = {0, 200, 0};
        block.draw_circle(x, y, 4, colorEnd);
    }

    cimg_library::CImgDisplay display(block);
    while(!display.is_closed()) {
        display.wait();
    }
}

};
