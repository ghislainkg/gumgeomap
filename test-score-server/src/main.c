#include "server/app_server.h"

int serverOnClientFunction(char * request, unsigned request_size, Buffer * response) {
	add_to_buffer(response, request, request_size);
	return 1;
}

int main(int argc, char *argv[]) {

	unsigned port;
	if(argc >= 2) {
		port = atoi(argv[1]);
	}
	else {
		port = 8081;
	}

	Server * server = server_create(port, serverOnClientFunction);
	server_start(server);

	return 0;
}
