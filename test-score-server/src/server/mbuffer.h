#ifndef _M_BUFFER_
#define _M_BUFFER_

#include <stdlib.h>
#include <string.h>

#include <stdio.h>

typedef struct Buffer {
	char * buf;
	size_t used_len;
	size_t len;
} Buffer;

Buffer create_buffer(size_t init_len);
void add_to_buffer(Buffer * buffer, char * str, size_t str_len);
void destroy_buffer(Buffer buffer);

#endif
