#include "app_server.h"

#define CHECK_FD_MODE_READ 0
#define CHECK_FD_MODE_WRITE 1

int check_fd(int fd, int sec, int micsec, int mode) {
	fd_set rfds;
    struct timeval tv;
    FD_ZERO(&rfds);
    FD_SET(fd, &rfds);
    tv.tv_sec = sec;
    tv.tv_usec = micsec;
   
    int retval;
    if(mode == 0) {
    	retval = select(fd+1, &rfds, NULL, NULL, &tv);
    }
    else {
    	retval = select(fd+1, NULL, &rfds, NULL, &tv);
    }
   
    if (retval == -1) {
        return -1;
    }
    else if (retval == 0) {
        return 0;
    }
    return 1;
}

Server * server_create(unsigned int port, ServerOnClientFunction serverOnClientFunction) {
  // Obtention du descripteur de fichier pour le socket serveur
	int server_socket_fd;
	server_socket_fd = socket(PF_INET6, SOCK_STREAM, 0);
	if (server_socket_fd == -1) {
		printf("Fail to create socket : %s\n", strerror(errno));
		return NULL;
	}
	// Option pour rendre le port accessible directement après l'avoir libérer
	int val = 1;
	int result_setsockopt = setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
	if(result_setsockopt == -1) {
		printf("Fail to set reuse address option : %s\n", strerror(errno));
		return NULL;
	}

	// Création d'un socket en IPv6
	struct sockaddr_in6 sin6;
	memset(&sin6, 0, sizeof(sin6));
	sin6.sin6_family = PF_INET6;
	sin6.sin6_port = htons(port);

	// On bind notre socket à notre port
	int rc = bind(server_socket_fd, (struct sockaddr *)&sin6, sizeof(sin6));
	if(rc < 0) {
		printf("Fail to bind socket : %s\n", strerror(errno));
		return NULL;
	}

	Server * server = (Server *) malloc(sizeof(Server));
	server->file_descriptor = server_socket_fd;
	memcpy(&(server->sin6), &sin6, sizeof(sin6));
	server->port = port;
	server->serverOnClientFunction = serverOnClientFunction;

	return server;
}

void server_destroy(Server * server) {
  int result_close = close(server->file_descriptor);
	if(result_close == -1){
    printf("Fail to close socket : %s\n", strerror(errno));
		printf("End\n");
	}
}

int server_start(Server * server) {
  int rc = listen(server->file_descriptor, 1024);
	if(rc < 0) {
		printf("Fail to listen : %s\n", strerror(errno));
		return -1;
	}

	while(1) {
		int client_socket = accept(server->file_descriptor, NULL, NULL);
		if(client_socket < 0) {
			printf("Fail to accept connexion : %s\n", strerror(errno));
			printf("End\n");
			continue;
		}

		printf("New connexion\n");

    	Buffer message = create_buffer(DEFAULT_MESSAGE_SIZE*2);
		while(1) {
			char * received_msg = new char[DEFAULT_MESSAGE_SIZE];
			int len = read(client_socket, received_msg, DEFAULT_MESSAGE_SIZE);
			if(len < 0) {
				printf("message not received : %s\n", strerror(errno));
				break;
			}

			add_to_buffer(&message, received_msg, len);
			delete received_msg;

			if(check_fd(client_socket, 0, 500, CHECK_FD_MODE_READ) == 0 || len == 0) {
				printf("Receive (%lu) : %s\n", message.used_len, message.buf);
				Buffer responseBuffer = create_buffer(DEFAULT_MESSAGE_SIZE);
				int resFunction = server->serverOnClientFunction(message.buf, message.used_len, &responseBuffer);
				printf("Response (%lu) : %s\n", responseBuffer.used_len, responseBuffer.buf);

				rc = write(client_socket, responseBuffer.buf, responseBuffer.used_len);
				if(rc < 0) {
					printf("Fail sending response (of size %lu) : %s\n", responseBuffer.used_len, responseBuffer.buf);
				}

				if(resFunction < 0) {
					return 0;
				}

				destroy_buffer(message);
				break;
			}
		}

		int rc = close(client_socket);
		if(rc == -1){
			printf("Fail to close socket : %s\n", strerror(errno));
		}
		printf("Connexion closed\n");
	}

	return 0;
}

