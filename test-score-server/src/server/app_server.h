#ifndef _APP_SERVER_
#define _APP_SERVER_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#include <cerrno>

#include "mbuffer.h"

#define DEFAULT_MESSAGE_SIZE 2048

typedef int (*ServerOnClientFunction)(char * request, unsigned request_size, Buffer * response);

typedef struct {
  	int file_descriptor;
	struct sockaddr_in6 sin6;
	unsigned int port;
	ServerOnClientFunction serverOnClientFunction;
} Server;

Server * server_create(unsigned int port, ServerOnClientFunction serverOnClientFunction);
void server_destroy(Server * server);
int server_start(Server * server);

#endif

