This projet originally is part of one of my school project at Telecom Paris.

# Description

The main purpose of this project is to provide a program capable of serving path for a specific request to go from one point to another. To achieve that, it uses a variety of third party libraries and cartographic data from OpenStreetMap.

# Dependencies

To compile the main program (contained in *./program*), you will need the folowing libraries installed on your computer (work well with a Linux operating system).

+ Curl
+ Pthread (package usualy installed on most linux distributions)

# Others third parties libraries

We also use some libraries already included in the repo.

+ [**CImg**](https://cimg.eu/) to display the map and path in a window.
+ [**Pugi**](https://pugixml.org/) to read Xml contents.
+ [**RapidJson**](https://rapidjson.org/) to parse and create json contents.

# Compile

The main program is located in the *./program* directory. Compile it in the following steps.

+ cd ./program
+ mkdir build
+ make

If those steps fullfil successfully, you should have a *map.out* executable in the *./program* directory. You can execute it to with as parameter a configuration file to start the server. Our default config file is located in *./program/execution-data/config.json*.

# The config file

The config file provides informations to the program. It's a json file add must contains all of the following fields.

Field about communication.
+ **inputsPortNumber** the port number to which the server listen to new path requests.
+ **scoreServerAddress** the address of the score server.
+ **scoreServerPort** the port of the score server.
+ **overpassUrl** the overpass server url (example : http://overpass.openstreetmap.fr/api/interpreter)

Field about display
+ **useDisplay** a boolean specifying if a window displaying the map and path should be open for each path request. On an operating system unable to display a window (like a server), this field should be keeped to *false*. (This field can be modified during the program runtime)

+ **defaultWaySpeed** a double specifying the default speed for roads where osm don't provide a usable maximum speed (the program doesn't see every way osm provide an autorised speed in a road). (This field can be modified during the program runtime)
+ **maxNodeCount** the maximum node count. When running, the program keep at a graph's matrix containing all osm nodes used for all previous path requests. It can be quite a lot of data. This field specifies a limit at which the program doesn't add anymore nodes to  the matrix. The bad result of this can be that we don't get enough node for request outside the zones covered by the graph.
+ **allocExtraSize**: the allocation size if the initial amount of memory space allocated for the graph's matrix. (1 for 1 node). you can considere *168 bytes* for each nodes, and *96* bytes for each pair of nodes. for *n* nodes, the matrix total size is 
	336n + 96n^2.
+ **nullDistanceApproximation** is the approximation of a null distance in meters during path search. two node distant of this value are considered at the same location. (This field can be modified during the program runtime)
+ **matrixBuildingThreadCount**: the amount of thread used to read data received from OpenStreetMap, it must not be 0. (This field can be modified during the program runtime). NB: This field should be keeped to 1 for now. There're some issues for a value greater than 1.

+ **LookSettingsNodeAmongInWayNode** a boolean specifying if the nodes corresponding the best to the coordinates of the start and end points of the path request should be part of a road. (This field can be modified during the program runtime)
+ **LookSettingsNodeAmongWayElementNode** a boolean specifying if the nodes corresponding the best to the coordinates of the start and end point of the request should be part a road element like crossing zones, trafic lights, ... . (This field can be modified during the program runtime)
+ **MaxDistanceForSettingsNodeSearch** a double specifying the maximum distance between the coordinates of the start and end points of the path request and the coordinate of the nodes in the graph we want to considere as start and end point during path search. If no node in the graph match that distance constraint, the program issue an http Overpass request to retrieve a part of the map (with getting unusefull data) containing nodes that match our constraint. (This field can be modified during the program runtime)

+ **WayElementAsExtraVoisin** a boolean. When performing the path search, it's possible that the graph isn't connexe, that can result in the start and end nodes are not linked by any path our a very long path in the graph. setting this field at true will tell the program to considere any node distance from **nullDistanceApproximation** as being linked. (This field can be modified during the program runtime)
+ **InWayAsExtraVoisin** a boolean specifying if the extra links are made only for node part of a road. (This field can be modified during the program runtime)


Example of configuration file:

	{
		"inputsPortNumber": 8080,
		"scoreServerAddress": "localhost",
		"scoreServerPort": 8081,
		"overpassUrl": "http://overpass.openstreetmap.fr/api/interpreter",

		"useDisplay": true,
		
		"defaultWaySpeed": 30.0,
		"maxNodeCount": 10000,
		"allocExtraSize": 15000,
		"nullDistanceApproximation": 1.0,
		"matrixBuildingThreadCount": 1,

		"LookSettingsNodeAmongInWayNode": true,
		"LookSettingsNodeAmongWayElementNode": true,
		"MaxDistanceForSettingsNodeSearch": 10000.0,

		"WayElementAsExtraVoisin": false,
		"InWayAsExtraVoisin": true
	}

# Communication with the client

The server must receives from any client an http request with a json containt.
That json must be formated as in the example bellow.

	{
		startLocation:  
		{  
			"osmId": 20,  
			"latitude": 48.712793,  
			"longitude": 2.199441  
		},  
		endLocation:  
		{  
			"osmId": 50,  
			"latitude": 48.7258,  
			"longitude": 2.2613  
		}  
	}

the osmId is the id of an osm node. If it's valid and contained in the matrix for both *startLocation* and *endLocation*, the program doesn't need more data and proceed directly to the path search.

At the end of the path search, the client receive json data formated as in this example:

	{  
		"rides":[  
			{  
				"distance":2413.03825879097,  
				"time":0.05698654096444449,  
				"directionPath":{  
					"path":[  
						{"latitude":48.7104163,"longitude":2.2195956,"speed":50.0,"osmNodeId":298676040},  
						{"latitude":48.7103334,"longitude":2.2194139,"speed":50.0,"osmNodeId":1341290058},  
						{"latitude":48.7102722,"longitude":2.2192591,"speed":50.0,"osmNodeId":1341290016}  
					]  
				}  
			},  
			{  
				"distance":2413.03825879097,  
				"time":0.05698654096444449,  
				"directionPath":{  
					"path":[  
						{"latitude":48.7104163,"longitude":2.2195956,"speed":50.0,"osmNodeId":298676040},  
						{"latitude":48.7103334,"longitude":2.2194139,"speed":50.0,"osmNodeId":1341290058},  
						{"latitude":48.7102722,"longitude":2.2192591,"speed":50.0,"osmNodeId":1341290016}  
					]  
				}  
			}  
		]  
	}  

# The score server

The program can communicate with another server that must return a single double (via TCP) and receive a json formated path.

	{  
		"distance":2413.03825879097,  
		"time":0.05698654096444449,  
		"directionPath":{  
			"path":[  
				{"latitude":48.7104163,"longitude":2.2195956,"speed":50.0,"osmNodeId":298676040},  
				{"latitude":48.7103334,"longitude":2.2194139,"speed":50.0,"osmNodeId":1341290058},  
				{"latitude":48.7102722,"longitude":2.2192591,"speed":50.0,"osmNodeId":1341290016}  
			]  
		}  
	}  


# Some test
There is a test client, in *./test-path-client*, and a test score server, in *./test-score-server*.
To run a test, after compiling the main program, compile the test programs in *./test-path-client* and *./test-score-server* (just go to the directory and run *make*).

+ Start the score server (port 8081) :
	
	cd ./test-score-server  
	./server.out  

+ Start the main program, with the example config file given above :

	cd ..  
	cd ./test-score-server  
	./program execution-data/config.json  

+ Start the client to issue a path request :
	
	cd ..  
	cd ./test-path-client  
	touch data/response.json  
	./client data/requests/telecomparis_centralsupelec.json data/response.json localhost 8080  

The request is issued and the main program run to find a path matching the request.

Make sure to have a low enough *maxNodeCount* in the config file so that your computer doesn't run out of memory and most the programs on your computer blocks. A good value to start with is 5000 (specificaly it's enough for the request in *data/requests/telecomparis_centralsupelec.json*).


# The developper

I am **Ghislain Kengne Gumete**, currently engineering student at **Telecom Paris**, one of the best french engineering school.
